{
    "id": "60f03c01-70ab-48a4-878b-35c175520c4e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_down_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70084ec5-3775-4283-bfb0-c3c6f0543d7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60f03c01-70ab-48a4-878b-35c175520c4e",
            "compositeImage": {
                "id": "6e044e81-0476-49ea-90bf-0c30c01f5e99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70084ec5-3775-4283-bfb0-c3c6f0543d7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d122579-f4e4-4cdf-8258-1473a97e4f84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70084ec5-3775-4283-bfb0-c3c6f0543d7b",
                    "LayerId": "4bcb579d-0e2a-4aa9-be94-a2ef01f581ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4bcb579d-0e2a-4aa9-be94-a2ef01f581ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60f03c01-70ab-48a4-878b-35c175520c4e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}