{
    "id": "4b843684-d440-4fbe-a0b3-e61f2cf8b992",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_left_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 6,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc55496d-e0d6-4ea0-80de-62cd19f67206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b843684-d440-4fbe-a0b3-e61f2cf8b992",
            "compositeImage": {
                "id": "94747020-d729-41ec-b283-080927a9b40b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc55496d-e0d6-4ea0-80de-62cd19f67206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed8f4cd0-ef89-40f6-b7a7-b1e145a977e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc55496d-e0d6-4ea0-80de-62cd19f67206",
                    "LayerId": "5ebeb37d-bae7-4c0e-8195-84f2639c4c13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5ebeb37d-bae7-4c0e-8195-84f2639c4c13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b843684-d440-4fbe-a0b3-e61f2cf8b992",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}