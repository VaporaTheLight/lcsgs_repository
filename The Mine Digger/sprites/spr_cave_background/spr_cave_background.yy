{
    "id": "63744b15-ea76-49bd-939a-f0ecfe1d3323",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cave_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 703,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09dd6a6f-0367-4d6f-a489-796eeaf6ca6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63744b15-ea76-49bd-939a-f0ecfe1d3323",
            "compositeImage": {
                "id": "e5427d75-151f-48eb-8458-9d668ae8aee4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09dd6a6f-0367-4d6f-a489-796eeaf6ca6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66b8d2b8-cfcd-437a-a25b-67973b22c86d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09dd6a6f-0367-4d6f-a489-796eeaf6ca6d",
                    "LayerId": "73165b13-b35a-422b-814c-944d6b6cdc43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 704,
    "layers": [
        {
            "id": "73165b13-b35a-422b-814c-944d6b6cdc43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63744b15-ea76-49bd-939a-f0ecfe1d3323",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}