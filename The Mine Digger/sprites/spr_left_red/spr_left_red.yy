{
    "id": "a45c0d28-b87d-400d-b64b-7e4a40cd9ba2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_left_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e66fe4db-b176-47ea-87ce-67f06cdabb8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45c0d28-b87d-400d-b64b-7e4a40cd9ba2",
            "compositeImage": {
                "id": "afc50d07-2d04-474b-a433-0f3948295e00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e66fe4db-b176-47ea-87ce-67f06cdabb8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25a15d49-50d4-4126-88c2-fc07e68c8904",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e66fe4db-b176-47ea-87ce-67f06cdabb8d",
                    "LayerId": "3e18d549-7c90-4c47-9e75-ac7147940f35"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3e18d549-7c90-4c47-9e75-ac7147940f35",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a45c0d28-b87d-400d-b64b-7e4a40cd9ba2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}