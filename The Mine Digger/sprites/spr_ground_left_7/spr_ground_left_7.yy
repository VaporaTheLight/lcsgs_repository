{
    "id": "72fd7a2c-e82f-405a-bed6-7f6e1d9cfc91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_left_7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92f3429f-554a-4cab-9e52-7250db652f68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72fd7a2c-e82f-405a-bed6-7f6e1d9cfc91",
            "compositeImage": {
                "id": "e286b3ad-a02c-4387-8e63-afa49ba61922",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92f3429f-554a-4cab-9e52-7250db652f68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4618af29-6aa0-4aec-aff9-b5e19eccd919",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92f3429f-554a-4cab-9e52-7250db652f68",
                    "LayerId": "583c1bf3-3d1d-43be-9422-66f0c8be76fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "583c1bf3-3d1d-43be-9422-66f0c8be76fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72fd7a2c-e82f-405a-bed6-7f6e1d9cfc91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}