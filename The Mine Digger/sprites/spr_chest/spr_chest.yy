{
    "id": "d0fcea88-79c9-4a56-99df-c19e64859ede",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_chest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ee83c0a-2bac-4125-a353-a1816b83c3a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0fcea88-79c9-4a56-99df-c19e64859ede",
            "compositeImage": {
                "id": "d9c8819c-d118-4dda-8198-cac09d7ae5f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ee83c0a-2bac-4125-a353-a1816b83c3a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f4029e8-0277-46d0-8e04-74b29a28316d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ee83c0a-2bac-4125-a353-a1816b83c3a7",
                    "LayerId": "1715cf74-b728-4668-8939-acf9fa910813"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1715cf74-b728-4668-8939-acf9fa910813",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0fcea88-79c9-4a56-99df-c19e64859ede",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}