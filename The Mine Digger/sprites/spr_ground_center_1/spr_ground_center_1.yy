{
    "id": "79166fb4-81ec-43a7-9abf-4b3f05ff8e35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_center_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f52e0cbe-cc7b-42d1-a54f-27896e98e117",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79166fb4-81ec-43a7-9abf-4b3f05ff8e35",
            "compositeImage": {
                "id": "51d2ac86-9ce8-413a-96b2-21539032c1c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f52e0cbe-cc7b-42d1-a54f-27896e98e117",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e506144a-699f-44a1-aa77-912ce4587b5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f52e0cbe-cc7b-42d1-a54f-27896e98e117",
                    "LayerId": "d03fd341-6479-418f-a0c9-325d7b83e127"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d03fd341-6479-418f-a0c9-325d7b83e127",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79166fb4-81ec-43a7-9abf-4b3f05ff8e35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}