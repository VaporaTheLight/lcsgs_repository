{
    "id": "934d4686-1406-474e-92ef-00441a16df15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_up_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39400164-5e9e-43a8-bdb7-2ec847b50012",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "934d4686-1406-474e-92ef-00441a16df15",
            "compositeImage": {
                "id": "3211cfe1-226f-456a-be33-c1fce64be142",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39400164-5e9e-43a8-bdb7-2ec847b50012",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78bb4b31-cd93-47aa-b8f7-1d38f89d8c3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39400164-5e9e-43a8-bdb7-2ec847b50012",
                    "LayerId": "93d6b202-edde-4d15-93f2-3c6a9070b762"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "93d6b202-edde-4d15-93f2-3c6a9070b762",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "934d4686-1406-474e-92ef-00441a16df15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}