{
    "id": "0162f0aa-275d-4353-a7d0-97112b2b57e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "adf85c44-757c-44c9-ae9e-02e2b7fb793a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0162f0aa-275d-4353-a7d0-97112b2b57e0",
            "compositeImage": {
                "id": "ab6e2f20-d338-4d7c-b02f-e74b29b545c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adf85c44-757c-44c9-ae9e-02e2b7fb793a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ed7d639-440f-46d4-9c0e-7ebaae46ab7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adf85c44-757c-44c9-ae9e-02e2b7fb793a",
                    "LayerId": "5480faad-f264-405f-af4a-bff05065c9e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5480faad-f264-405f-af4a-bff05065c9e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0162f0aa-275d-4353-a7d0-97112b2b57e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}