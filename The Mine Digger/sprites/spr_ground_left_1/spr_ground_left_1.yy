{
    "id": "332f0776-a25c-4f39-be1f-1d6004b27b1c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_left_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ba83a77-490c-496c-84ca-1d2083f34c9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "332f0776-a25c-4f39-be1f-1d6004b27b1c",
            "compositeImage": {
                "id": "9ea06572-3d2b-4eec-b58b-408359bc637a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ba83a77-490c-496c-84ca-1d2083f34c9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "392c4640-c338-45a9-b730-6e99f592f092",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ba83a77-490c-496c-84ca-1d2083f34c9a",
                    "LayerId": "014d4de4-b137-495e-bde7-8e13d1d4652c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "014d4de4-b137-495e-bde7-8e13d1d4652c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "332f0776-a25c-4f39-be1f-1d6004b27b1c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}