{
    "id": "3d0d08d0-a000-4819-8fe2-ec1e26ca4277",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_alert_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "855638af-9627-488e-85bb-1f67e14bc860",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d0d08d0-a000-4819-8fe2-ec1e26ca4277",
            "compositeImage": {
                "id": "e706e0e0-3f2e-4926-b52b-d8401a590eb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "855638af-9627-488e-85bb-1f67e14bc860",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63505ed3-1a2c-4481-9406-4a302c901913",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "855638af-9627-488e-85bb-1f67e14bc860",
                    "LayerId": "082b01dc-a249-4834-9701-9d76f2cba510"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "082b01dc-a249-4834-9701-9d76f2cba510",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d0d08d0-a000-4819-8fe2-ec1e26ca4277",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}