{
    "id": "de23d0e3-de8f-4445-ad0f-e2d123814637",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_left_8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a340a097-9254-468c-b289-d3adb6541ba4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de23d0e3-de8f-4445-ad0f-e2d123814637",
            "compositeImage": {
                "id": "06cfcef0-19e8-41de-9860-ee6c8ba99958",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a340a097-9254-468c-b289-d3adb6541ba4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "465965d7-4a90-4b0c-8cc5-1f678dfc6f15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a340a097-9254-468c-b289-d3adb6541ba4",
                    "LayerId": "941432ab-36ef-4cb7-8f0f-0926dfdb45a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "941432ab-36ef-4cb7-8f0f-0926dfdb45a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de23d0e3-de8f-4445-ad0f-e2d123814637",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}