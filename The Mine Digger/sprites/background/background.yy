{
    "id": "3fe24d32-bce2-4239-9d69-d175c8ac0c94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20e82adf-ca65-4115-9785-cf7e6edd0fc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fe24d32-bce2-4239-9d69-d175c8ac0c94",
            "compositeImage": {
                "id": "f12ea566-2820-41e7-a7ce-923e4fb6281a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20e82adf-ca65-4115-9785-cf7e6edd0fc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0792a23-e9e7-4747-87c0-54c722a71687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20e82adf-ca65-4115-9785-cf7e6edd0fc1",
                    "LayerId": "5c5ffa77-6413-4494-97d6-f7025d52442b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5c5ffa77-6413-4494-97d6-f7025d52442b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3fe24d32-bce2-4239-9d69-d175c8ac0c94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}