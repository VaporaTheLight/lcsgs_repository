{
    "id": "90307d10-1cd1-420c-a7a5-6314063b58b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_bottom_left_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77bcc083-0a59-47c6-958f-7a4cfcedb143",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90307d10-1cd1-420c-a7a5-6314063b58b4",
            "compositeImage": {
                "id": "99b4dde6-440f-472a-8814-2759fb33cb60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77bcc083-0a59-47c6-958f-7a4cfcedb143",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "357908d0-7f97-4c05-b79c-f55a04d8fa01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77bcc083-0a59-47c6-958f-7a4cfcedb143",
                    "LayerId": "e9ef73e7-e5c2-46db-8822-9014b28b7fb5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e9ef73e7-e5c2-46db-8822-9014b28b7fb5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90307d10-1cd1-420c-a7a5-6314063b58b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}