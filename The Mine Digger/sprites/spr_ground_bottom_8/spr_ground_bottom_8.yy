{
    "id": "8534a8aa-299b-4c14-8a5f-17a1fdf40b8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_bottom_8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91ed91a8-f370-4a4a-b807-1df01fdc447c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8534a8aa-299b-4c14-8a5f-17a1fdf40b8e",
            "compositeImage": {
                "id": "5834674e-0f48-43c7-8acd-110a77749f61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91ed91a8-f370-4a4a-b807-1df01fdc447c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73342b7c-6a95-419d-9416-00184446533f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91ed91a8-f370-4a4a-b807-1df01fdc447c",
                    "LayerId": "658744a8-642f-47f6-b128-b990e63df62b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "658744a8-642f-47f6-b128-b990e63df62b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8534a8aa-299b-4c14-8a5f-17a1fdf40b8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}