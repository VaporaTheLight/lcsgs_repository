{
    "id": "fbab1120-3a0d-4b2a-b1cc-04749a8ba362",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_right_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f214638-fbcc-471f-8fdf-011fa2814c89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbab1120-3a0d-4b2a-b1cc-04749a8ba362",
            "compositeImage": {
                "id": "fc7344d0-f4f2-4e28-b311-286253b94fc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f214638-fbcc-471f-8fdf-011fa2814c89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c40802b3-8f29-4b4e-92b2-6039650c755f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f214638-fbcc-471f-8fdf-011fa2814c89",
                    "LayerId": "d7325021-dc11-4728-ac61-f28344ef110a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d7325021-dc11-4728-ac61-f28344ef110a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbab1120-3a0d-4b2a-b1cc-04749a8ba362",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}