{
    "id": "f90ad642-7b9b-4730-94a0-9e5cd1f3d00c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_center_11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93c88326-8539-4566-a80a-ff5bf6ea218c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f90ad642-7b9b-4730-94a0-9e5cd1f3d00c",
            "compositeImage": {
                "id": "e791389c-b28a-4f34-92e6-9ad7215ef47f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93c88326-8539-4566-a80a-ff5bf6ea218c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7eea4452-4de3-492e-b5a4-3dedfafe5650",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93c88326-8539-4566-a80a-ff5bf6ea218c",
                    "LayerId": "be61e419-79f2-4229-972d-96aca217dc6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "be61e419-79f2-4229-972d-96aca217dc6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f90ad642-7b9b-4730-94a0-9e5cd1f3d00c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}