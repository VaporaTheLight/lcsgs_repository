{
    "id": "6597dadc-ee00-40f6-94bf-afc9f1a66b6c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_bottom_right_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15f2ce74-6987-4a56-a32e-b6036380edf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597dadc-ee00-40f6-94bf-afc9f1a66b6c",
            "compositeImage": {
                "id": "a958b1b0-3dbd-41df-af4f-292655dd7c8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15f2ce74-6987-4a56-a32e-b6036380edf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a41de045-7eba-4d78-ac0d-29f439ce4a05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15f2ce74-6987-4a56-a32e-b6036380edf7",
                    "LayerId": "03f524ed-36e4-457e-be2f-d7965e345827"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "03f524ed-36e4-457e-be2f-d7965e345827",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6597dadc-ee00-40f6-94bf-afc9f1a66b6c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}