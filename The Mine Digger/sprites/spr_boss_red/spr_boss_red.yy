{
    "id": "9421701a-eec4-453c-9034-aae028e893a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "485ada69-6612-4533-81b5-ff78f238c975",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9421701a-eec4-453c-9034-aae028e893a0",
            "compositeImage": {
                "id": "5a6f5617-1d1d-4ff5-8ef0-9c407575d330",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "485ada69-6612-4533-81b5-ff78f238c975",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffdb6538-66c6-4685-bde0-71435dec9a64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "485ada69-6612-4533-81b5-ff78f238c975",
                    "LayerId": "9a72f580-b214-4d26-8230-7e604e2a7132"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9a72f580-b214-4d26-8230-7e604e2a7132",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9421701a-eec4-453c-9034-aae028e893a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}