{
    "id": "96e7fc00-957e-475c-98af-8e0cf7c2816b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_left_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4db4967c-b2d3-4d54-b032-51351e496555",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96e7fc00-957e-475c-98af-8e0cf7c2816b",
            "compositeImage": {
                "id": "835f0a41-bfe4-4473-adc3-0636e2bbe935",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4db4967c-b2d3-4d54-b032-51351e496555",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e85daad1-16ab-47ef-9bf2-7e52e1dc1d97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4db4967c-b2d3-4d54-b032-51351e496555",
                    "LayerId": "7485bced-cb94-48dd-bcc6-4453d0ce0f59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7485bced-cb94-48dd-bcc6-4453d0ce0f59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96e7fc00-957e-475c-98af-8e0cf7c2816b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}