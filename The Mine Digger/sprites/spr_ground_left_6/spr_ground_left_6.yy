{
    "id": "7023eaf8-0a74-48be-a2db-457a19f14f22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_left_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 5,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36a55850-935b-4491-8608-50bd0ec1ebb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7023eaf8-0a74-48be-a2db-457a19f14f22",
            "compositeImage": {
                "id": "1832ce16-c7d0-465f-b731-8cf1d14f3349",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36a55850-935b-4491-8608-50bd0ec1ebb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af3ebf77-6ce2-4a0d-99d3-f98648a9b115",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36a55850-935b-4491-8608-50bd0ec1ebb9",
                    "LayerId": "33dd2d30-8fdb-476e-8eaa-77737fa47aa9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "33dd2d30-8fdb-476e-8eaa-77737fa47aa9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7023eaf8-0a74-48be-a2db-457a19f14f22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}