{
    "id": "f0280aad-d4ed-4ea0-9cb6-11ce2f8bb9ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_right_9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bcbaa133-e71a-4800-9fab-876f852ecb57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0280aad-d4ed-4ea0-9cb6-11ce2f8bb9ba",
            "compositeImage": {
                "id": "443f408d-6e2b-40ab-867f-34c1f1ecc95a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcbaa133-e71a-4800-9fab-876f852ecb57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48314a96-1193-4b82-a57f-6491eb150ed2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcbaa133-e71a-4800-9fab-876f852ecb57",
                    "LayerId": "839a013d-98f8-4caf-9d11-7ce383a226fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "839a013d-98f8-4caf-9d11-7ce383a226fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0280aad-d4ed-4ea0-9cb6-11ce2f8bb9ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}