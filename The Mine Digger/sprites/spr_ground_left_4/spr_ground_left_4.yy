{
    "id": "aec16efe-c9e7-4ca4-be7b-004f0e70c5cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_left_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 6,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b03af31-7934-4b40-ad23-acf7630f362e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aec16efe-c9e7-4ca4-be7b-004f0e70c5cb",
            "compositeImage": {
                "id": "8ce022fe-1d55-4790-a4e5-1376cd1f8f37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b03af31-7934-4b40-ad23-acf7630f362e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8c101c7-5927-4bcd-b925-a75caabf0724",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b03af31-7934-4b40-ad23-acf7630f362e",
                    "LayerId": "baa926df-ddc0-46ca-bbf2-c27f9f1f9722"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "baa926df-ddc0-46ca-bbf2-c27f9f1f9722",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aec16efe-c9e7-4ca4-be7b-004f0e70c5cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}