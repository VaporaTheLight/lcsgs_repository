{
    "id": "3441a804-dd2a-46b9-914c-3dfbb9773762",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blue_gem",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29f0dc93-76ae-4ab8-b27e-b7ffda78462b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3441a804-dd2a-46b9-914c-3dfbb9773762",
            "compositeImage": {
                "id": "543f4d86-9bb1-4b95-9a4c-489b9333cf20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29f0dc93-76ae-4ab8-b27e-b7ffda78462b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3a2e40a-5de0-430a-9ba7-115e3b786133",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29f0dc93-76ae-4ab8-b27e-b7ffda78462b",
                    "LayerId": "a88b98dc-41d8-4869-894b-efd58189faa8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a88b98dc-41d8-4869-894b-efd58189faa8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3441a804-dd2a-46b9-914c-3dfbb9773762",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}