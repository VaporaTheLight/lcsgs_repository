{
    "id": "e99e8aa9-82a1-4ab7-931c-c0fcd30dc07c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_right_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21f2ca02-e144-450a-a2bd-f04dbe691b25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e99e8aa9-82a1-4ab7-931c-c0fcd30dc07c",
            "compositeImage": {
                "id": "3af0dbf3-e255-4f20-be4c-46397757c71f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21f2ca02-e144-450a-a2bd-f04dbe691b25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21076a6c-ee59-4425-a36c-2b13ba733fb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21f2ca02-e144-450a-a2bd-f04dbe691b25",
                    "LayerId": "12d9cad8-bd63-47c1-96e0-bef68a1bf52b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "12d9cad8-bd63-47c1-96e0-bef68a1bf52b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e99e8aa9-82a1-4ab7-931c-c0fcd30dc07c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}