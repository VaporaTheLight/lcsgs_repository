{
    "id": "c98968f6-013b-4117-b375-594a54db8a51",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_left_10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c6d7588-e5d4-43f2-b96a-e79abe97da77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c98968f6-013b-4117-b375-594a54db8a51",
            "compositeImage": {
                "id": "415240fb-ba40-4a9c-89fa-3d15c7a154f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c6d7588-e5d4-43f2-b96a-e79abe97da77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a36d74b-bc37-40c5-9172-e3f3ee609d68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c6d7588-e5d4-43f2-b96a-e79abe97da77",
                    "LayerId": "7f4afae4-6a77-4c5f-970a-f654e2312627"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7f4afae4-6a77-4c5f-970a-f654e2312627",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c98968f6-013b-4117-b375-594a54db8a51",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}