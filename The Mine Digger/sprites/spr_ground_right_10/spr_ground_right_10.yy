{
    "id": "b7c0905d-950c-4741-be70-00e87d13cad5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_right_10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c671ae7-511e-446c-8e51-97de46d92fba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7c0905d-950c-4741-be70-00e87d13cad5",
            "compositeImage": {
                "id": "f9a133fa-83ff-4b99-8e87-5e4b9ae5e6ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c671ae7-511e-446c-8e51-97de46d92fba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0903e5e-3664-4e40-81d0-c67ef3eb459c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c671ae7-511e-446c-8e51-97de46d92fba",
                    "LayerId": "fc3e962c-1922-49d4-bc2e-8451befec100"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fc3e962c-1922-49d4-bc2e-8451befec100",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7c0905d-950c-4741-be70-00e87d13cad5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}