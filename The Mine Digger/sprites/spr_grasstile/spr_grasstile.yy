{
    "id": "f8e3b85e-4c0d-44ab-a234-7f0b9d852e37",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grasstile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87b07e35-d71e-4816-80ba-49f71e842bf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e3b85e-4c0d-44ab-a234-7f0b9d852e37",
            "compositeImage": {
                "id": "557114f8-8013-4453-ba61-6304279cbccf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87b07e35-d71e-4816-80ba-49f71e842bf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "417bc981-9916-4e49-b475-3f6101db60fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87b07e35-d71e-4816-80ba-49f71e842bf7",
                    "LayerId": "24b34df6-0367-477f-a997-45855c5baf7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "24b34df6-0367-477f-a997-45855c5baf7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8e3b85e-4c0d-44ab-a234-7f0b9d852e37",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": -55,
    "yorig": 46
}