{
    "id": "457aa688-5e9a-4036-8230-d4fce10adce6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_left_12",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81ac8c69-8a5b-4e23-9e98-44b854ab38a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "457aa688-5e9a-4036-8230-d4fce10adce6",
            "compositeImage": {
                "id": "d7658e61-5f70-48a1-8681-2683e65aca41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81ac8c69-8a5b-4e23-9e98-44b854ab38a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "373f8d62-f1a8-4069-8df8-81e20ddade77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81ac8c69-8a5b-4e23-9e98-44b854ab38a7",
                    "LayerId": "efdf7180-3a10-4e03-9a85-c275904d7625"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "efdf7180-3a10-4e03-9a85-c275904d7625",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "457aa688-5e9a-4036-8230-d4fce10adce6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}