{
    "id": "4b408a48-e157-46c6-ab75-e285b5452f3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_miner_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84737997-73ea-4ffd-98f4-d35608969f36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b408a48-e157-46c6-ab75-e285b5452f3c",
            "compositeImage": {
                "id": "1ce59885-1eb3-4827-a551-f6c994a46b25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84737997-73ea-4ffd-98f4-d35608969f36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02f27285-fd1e-4f23-bc0e-804e566f6832",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84737997-73ea-4ffd-98f4-d35608969f36",
                    "LayerId": "1f1f284f-bc19-466c-a9b5-52df2ef0f416"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1f1f284f-bc19-466c-a9b5-52df2ef0f416",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b408a48-e157-46c6-ab75-e285b5452f3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}