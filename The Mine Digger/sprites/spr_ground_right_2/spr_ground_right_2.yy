{
    "id": "7d0737dd-8edd-4e2e-9546-3c5dc7619c97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_right_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69da1280-dc92-4889-a231-a05f0f88aa55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d0737dd-8edd-4e2e-9546-3c5dc7619c97",
            "compositeImage": {
                "id": "612fddc3-abf4-4395-af93-bdeb21301de6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69da1280-dc92-4889-a231-a05f0f88aa55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45bf0da7-b818-4237-aff3-c7f1def526f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69da1280-dc92-4889-a231-a05f0f88aa55",
                    "LayerId": "b1d5f7b1-b26b-4fda-b64d-e794f3acd855"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b1d5f7b1-b26b-4fda-b64d-e794f3acd855",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d0737dd-8edd-4e2e-9546-3c5dc7619c97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}