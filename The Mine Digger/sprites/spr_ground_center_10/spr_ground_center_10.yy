{
    "id": "29fe9fe0-4eb8-4e3a-8aa5-4181738bc426",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_center_10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a2e30c7-36e2-493d-a088-bde7c4cb093a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29fe9fe0-4eb8-4e3a-8aa5-4181738bc426",
            "compositeImage": {
                "id": "3625ed27-bca0-4368-837b-99a0e3bce88d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a2e30c7-36e2-493d-a088-bde7c4cb093a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78391ec8-1bc1-4617-a0f4-c366f055c5fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a2e30c7-36e2-493d-a088-bde7c4cb093a",
                    "LayerId": "536e7555-32b6-43de-ae07-148f9dc7817a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "536e7555-32b6-43de-ae07-148f9dc7817a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29fe9fe0-4eb8-4e3a-8aa5-4181738bc426",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}