{
    "id": "74f1af80-5a98-4d38-9137-7d9c4df42bd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_left_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 5,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e0bc9d2-4592-418c-87ca-c59c2973e394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74f1af80-5a98-4d38-9137-7d9c4df42bd3",
            "compositeImage": {
                "id": "d49f1c1d-9905-48c3-90fe-ed7d5eda0094",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e0bc9d2-4592-418c-87ca-c59c2973e394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d54a6a5c-930b-45a6-a88f-a3f0bc8bd2e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e0bc9d2-4592-418c-87ca-c59c2973e394",
                    "LayerId": "967f08f0-adb8-48d4-87a9-2cf74e12f0b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "967f08f0-adb8-48d4-87a9-2cf74e12f0b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74f1af80-5a98-4d38-9137-7d9c4df42bd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}