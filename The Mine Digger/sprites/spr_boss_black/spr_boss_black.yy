{
    "id": "99355de9-120d-4011-8a48-81d16c43f4f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c67dd8a3-6fb4-40ef-ac13-a424cbb2ef1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99355de9-120d-4011-8a48-81d16c43f4f5",
            "compositeImage": {
                "id": "0efad37f-b633-497b-a304-c05b95768908",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c67dd8a3-6fb4-40ef-ac13-a424cbb2ef1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5e6b954-16ee-4a75-bc2d-35b06afc3be0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c67dd8a3-6fb4-40ef-ac13-a424cbb2ef1a",
                    "LayerId": "98d6babd-ca4c-47a0-8f00-8d243d07c785"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "98d6babd-ca4c-47a0-8f00-8d243d07c785",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99355de9-120d-4011-8a48-81d16c43f4f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}