{
    "id": "17829dd7-e9c2-47c1-a863-67d0043d967b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_right_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "882dd6b7-eb22-4ad9-b4ed-c6e7ce4635fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17829dd7-e9c2-47c1-a863-67d0043d967b",
            "compositeImage": {
                "id": "3912fa50-ea22-484b-8640-4ce6453b5585",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "882dd6b7-eb22-4ad9-b4ed-c6e7ce4635fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36641232-8300-4e37-8cb4-7fcaad28709a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "882dd6b7-eb22-4ad9-b4ed-c6e7ce4635fc",
                    "LayerId": "2cf2f8ab-3645-45ba-90e8-94d4b0abd013"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2cf2f8ab-3645-45ba-90e8-94d4b0abd013",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17829dd7-e9c2-47c1-a863-67d0043d967b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}