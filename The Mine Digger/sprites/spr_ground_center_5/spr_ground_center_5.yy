{
    "id": "ec99f3c8-0a15-4254-8558-be60c255fb56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_center_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6a2d195-1db9-4845-9509-1abe9ed2a74d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec99f3c8-0a15-4254-8558-be60c255fb56",
            "compositeImage": {
                "id": "df6f6e0c-9261-41e1-8867-adc9a958720d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6a2d195-1db9-4845-9509-1abe9ed2a74d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7df4ab54-5448-4904-b8f3-64aff4f42202",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6a2d195-1db9-4845-9509-1abe9ed2a74d",
                    "LayerId": "5123c40d-dc3b-4b08-865d-5352edd12f7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5123c40d-dc3b-4b08-865d-5352edd12f7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec99f3c8-0a15-4254-8558-be60c255fb56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}