{
    "id": "a97c411c-156a-40b9-abc6-36ea365f80aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_bottom_7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f72002b-e2b9-4d9b-be94-214f29c3823d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a97c411c-156a-40b9-abc6-36ea365f80aa",
            "compositeImage": {
                "id": "bad4d6aa-721d-4f02-8f14-f6194ae2da21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f72002b-e2b9-4d9b-be94-214f29c3823d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13a0e431-8cd9-460c-aa23-ecf541ef06c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f72002b-e2b9-4d9b-be94-214f29c3823d",
                    "LayerId": "75dcf863-d3ab-4cb1-b649-24a11eb242cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "75dcf863-d3ab-4cb1-b649-24a11eb242cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a97c411c-156a-40b9-abc6-36ea365f80aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}