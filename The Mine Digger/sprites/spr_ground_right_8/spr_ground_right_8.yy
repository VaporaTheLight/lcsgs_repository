{
    "id": "f30ce602-5d51-45ce-abfa-c0a6a13e9360",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_right_8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a6179a8-626e-4118-812d-fb2f633142b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f30ce602-5d51-45ce-abfa-c0a6a13e9360",
            "compositeImage": {
                "id": "a9755c7d-b722-4371-a2a2-245ee631e281",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a6179a8-626e-4118-812d-fb2f633142b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51a555b0-2ef3-470d-b9b6-a526f5726ae7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a6179a8-626e-4118-812d-fb2f633142b4",
                    "LayerId": "b06a6aaa-bc51-4fe9-a390-7a084cee1986"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b06a6aaa-bc51-4fe9-a390-7a084cee1986",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f30ce602-5d51-45ce-abfa-c0a6a13e9360",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}