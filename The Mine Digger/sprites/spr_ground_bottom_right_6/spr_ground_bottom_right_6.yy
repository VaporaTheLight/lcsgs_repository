{
    "id": "22e0e956-d2f8-438e-adc9-d6248b82bdc0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_bottom_right_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69cdff78-8d3e-4b7c-8819-1a14b03f7051",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22e0e956-d2f8-438e-adc9-d6248b82bdc0",
            "compositeImage": {
                "id": "c93cf555-298f-478f-85fa-6ccd6eb9a8d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69cdff78-8d3e-4b7c-8819-1a14b03f7051",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc4d3185-c259-44cd-ae16-527d993e80ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69cdff78-8d3e-4b7c-8819-1a14b03f7051",
                    "LayerId": "56c2803c-5ed6-485a-a1b8-ff37c71aac52"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "56c2803c-5ed6-485a-a1b8-ff37c71aac52",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22e0e956-d2f8-438e-adc9-d6248b82bdc0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}