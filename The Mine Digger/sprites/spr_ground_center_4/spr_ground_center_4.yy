{
    "id": "d70c8aaa-e9c0-4940-aee3-a5cb9e7748aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_center_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25130938-1bf2-4b62-9815-63af3a5047c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d70c8aaa-e9c0-4940-aee3-a5cb9e7748aa",
            "compositeImage": {
                "id": "8b8a473a-e40d-4d85-85fe-abb3855a792b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25130938-1bf2-4b62-9815-63af3a5047c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cab24925-248c-454d-84f1-eb713f833b63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25130938-1bf2-4b62-9815-63af3a5047c0",
                    "LayerId": "0a326c97-0843-4c62-aaab-c9e0aaba5429"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0a326c97-0843-4c62-aaab-c9e0aaba5429",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d70c8aaa-e9c0-4940-aee3-a5cb9e7748aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}