{
    "id": "1847a1c7-66b0-4289-b2b7-f551e58587f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_right_12",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0d46a9e-f72a-4195-b282-6d41396afc0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1847a1c7-66b0-4289-b2b7-f551e58587f5",
            "compositeImage": {
                "id": "126665c4-7e31-4e28-a37e-b9ddb00542cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0d46a9e-f72a-4195-b282-6d41396afc0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1a557e9-7896-4054-b63a-056b675427be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0d46a9e-f72a-4195-b282-6d41396afc0a",
                    "LayerId": "51e193d2-e32e-4b6c-97f2-ad96a9613a17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "51e193d2-e32e-4b6c-97f2-ad96a9613a17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1847a1c7-66b0-4289-b2b7-f551e58587f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}