{
    "id": "aeed01c4-c86b-43b6-a02c-3d8bccc1b963",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sQuitBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d03303aa-0c5f-4872-b52a-9314bafcd15a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aeed01c4-c86b-43b6-a02c-3d8bccc1b963",
            "compositeImage": {
                "id": "7c2c3438-4b94-4308-a07d-5c385bd9d20e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d03303aa-0c5f-4872-b52a-9314bafcd15a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cfc33ab-b3fa-4757-adfe-113f123fd347",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d03303aa-0c5f-4872-b52a-9314bafcd15a",
                    "LayerId": "0caf9427-dd24-432f-8625-e4aeb3ac55b0"
                }
            ]
        },
        {
            "id": "85789c16-ba4d-456e-a600-8b7ee91d4911",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aeed01c4-c86b-43b6-a02c-3d8bccc1b963",
            "compositeImage": {
                "id": "c4181190-6123-4b80-b41d-57b9d4d02fa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85789c16-ba4d-456e-a600-8b7ee91d4911",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07add307-131a-48d1-9aaf-3a364f0051c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85789c16-ba4d-456e-a600-8b7ee91d4911",
                    "LayerId": "0caf9427-dd24-432f-8625-e4aeb3ac55b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "0caf9427-dd24-432f-8625-e4aeb3ac55b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aeed01c4-c86b-43b6-a02c-3d8bccc1b963",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}