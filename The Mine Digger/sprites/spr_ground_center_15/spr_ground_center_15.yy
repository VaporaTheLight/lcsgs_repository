{
    "id": "a1671d97-988f-4070-8a9d-fedb095e65ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_center_15",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c4fb3ce-cf75-4a37-b2fd-5550c1b9bfcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1671d97-988f-4070-8a9d-fedb095e65ae",
            "compositeImage": {
                "id": "ca8ea4fa-a66e-4908-ad73-2c388cff73c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c4fb3ce-cf75-4a37-b2fd-5550c1b9bfcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ba99d93-47e9-4ccc-9f66-b363e82311c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c4fb3ce-cf75-4a37-b2fd-5550c1b9bfcf",
                    "LayerId": "1b3e26ea-9b10-48c7-9ec3-44eabdf5c19c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1b3e26ea-9b10-48c7-9ec3-44eabdf5c19c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1671d97-988f-4070-8a9d-fedb095e65ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}