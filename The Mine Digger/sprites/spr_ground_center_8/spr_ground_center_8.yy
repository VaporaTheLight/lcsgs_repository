{
    "id": "7fc3c46a-37e5-41f1-a447-c8b59098c995",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_center_8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e43c6cb-c626-4ba8-bde5-070245116326",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fc3c46a-37e5-41f1-a447-c8b59098c995",
            "compositeImage": {
                "id": "da5f25df-f0d9-4ad9-a898-786dc1dbfd7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e43c6cb-c626-4ba8-bde5-070245116326",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6266d86-4c2a-4311-8b1f-493bb8044a3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e43c6cb-c626-4ba8-bde5-070245116326",
                    "LayerId": "426f4102-513a-4d44-9ada-a271d5b93543"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "426f4102-513a-4d44-9ada-a271d5b93543",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7fc3c46a-37e5-41f1-a447-c8b59098c995",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}