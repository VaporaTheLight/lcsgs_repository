{
    "id": "a3faf60a-bb08-481c-92e1-c3d3cb367a63",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_miner_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f56b34a9-c421-493b-b81c-e958c11e7aae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3faf60a-bb08-481c-92e1-c3d3cb367a63",
            "compositeImage": {
                "id": "43d4e651-9d40-4221-88fb-69ef8a3a991b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f56b34a9-c421-493b-b81c-e958c11e7aae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19865f50-f0c1-48aa-b7a0-cf9e73ea2291",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f56b34a9-c421-493b-b81c-e958c11e7aae",
                    "LayerId": "3531b679-f3e4-41a2-b2c5-9f73ce21bf6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3531b679-f3e4-41a2-b2c5-9f73ce21bf6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3faf60a-bb08-481c-92e1-c3d3cb367a63",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}