{
    "id": "0146f7e6-7fbc-4b47-9cd0-8a5eed668996",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_right_7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d0d9c1e-0ea3-4c33-91c0-792b182f761d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0146f7e6-7fbc-4b47-9cd0-8a5eed668996",
            "compositeImage": {
                "id": "7f6a923f-eaa3-4795-a9e1-428817b9f1a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d0d9c1e-0ea3-4c33-91c0-792b182f761d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b086280d-3d74-4ab6-9ca7-ad6d46477c8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d0d9c1e-0ea3-4c33-91c0-792b182f761d",
                    "LayerId": "739bb698-e283-4517-85be-a95a41a06079"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "739bb698-e283-4517-85be-a95a41a06079",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0146f7e6-7fbc-4b47-9cd0-8a5eed668996",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}