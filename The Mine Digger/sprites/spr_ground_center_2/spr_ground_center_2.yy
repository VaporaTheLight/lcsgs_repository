{
    "id": "e460d3cf-6bf0-40c5-82d7-9b89d6292e78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_center_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03ef5a74-9e8a-499a-92fa-49d3c93f234f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e460d3cf-6bf0-40c5-82d7-9b89d6292e78",
            "compositeImage": {
                "id": "bc9bd63d-8876-42ab-b2c8-ba663cad3696",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03ef5a74-9e8a-499a-92fa-49d3c93f234f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30c57f5e-969f-478b-8444-f5d3f6973a3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03ef5a74-9e8a-499a-92fa-49d3c93f234f",
                    "LayerId": "a99f15d7-28d5-4997-be3e-e34109c498de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a99f15d7-28d5-4997-be3e-e34109c498de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e460d3cf-6bf0-40c5-82d7-9b89d6292e78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}