{
    "id": "b3ffe12b-8f3e-4558-9117-47786eb8180f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spike_crusher",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd25fde3-bea5-4568-b827-0de8d858a139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ffe12b-8f3e-4558-9117-47786eb8180f",
            "compositeImage": {
                "id": "6763e564-2040-4a2e-a294-980d777d3a4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd25fde3-bea5-4568-b827-0de8d858a139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb6b60b6-4a6c-44d2-84ae-c3d0d13692f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd25fde3-bea5-4568-b827-0de8d858a139",
                    "LayerId": "cac0b6ef-2b24-408a-8391-42609c4601d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cac0b6ef-2b24-408a-8391-42609c4601d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3ffe12b-8f3e-4558-9117-47786eb8180f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 16
}