{
    "id": "4568ef67-b40c-4674-ae7d-d231943bd087",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_box_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3f74394-b9ac-4b75-ba5c-23b3d983bc00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4568ef67-b40c-4674-ae7d-d231943bd087",
            "compositeImage": {
                "id": "963c51b9-9965-4813-8959-d12e03288e27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3f74394-b9ac-4b75-ba5c-23b3d983bc00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7bbd727-fdb0-4bce-adbd-b550e0771fba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3f74394-b9ac-4b75-ba5c-23b3d983bc00",
                    "LayerId": "d678971f-3a0d-43dd-b3a0-2f282586f1ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d678971f-3a0d-43dd-b3a0-2f282586f1ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4568ef67-b40c-4674-ae7d-d231943bd087",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}