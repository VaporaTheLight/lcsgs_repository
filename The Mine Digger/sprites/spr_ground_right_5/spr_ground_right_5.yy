{
    "id": "9552b0d5-49ee-46d8-9b68-f578558457d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_right_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f35e992a-6758-4098-be82-142dd2cba51b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9552b0d5-49ee-46d8-9b68-f578558457d4",
            "compositeImage": {
                "id": "4fe0f5c1-781c-4db9-ae56-ffa47d11a9e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f35e992a-6758-4098-be82-142dd2cba51b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b613dec-89eb-4752-bf98-bd2aef87259a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f35e992a-6758-4098-be82-142dd2cba51b",
                    "LayerId": "36844cbc-c68c-41e6-9dd5-ca32dc54f3ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "36844cbc-c68c-41e6-9dd5-ca32dc54f3ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9552b0d5-49ee-46d8-9b68-f578558457d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}