{
    "id": "daa9365e-9e3e-4ee5-ac6a-bcad12177903",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e40c6100-5954-4e5e-9e85-ed1ab02a07e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daa9365e-9e3e-4ee5-ac6a-bcad12177903",
            "compositeImage": {
                "id": "92da26c8-5966-43c6-b54e-f5a6d2237409",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e40c6100-5954-4e5e-9e85-ed1ab02a07e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42b297a7-baa7-4fde-a8df-8d1383427427",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e40c6100-5954-4e5e-9e85-ed1ab02a07e4",
                    "LayerId": "2cab0fd4-3ecb-4c15-8a45-0dbd2ce41ce9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2cab0fd4-3ecb-4c15-8a45-0dbd2ce41ce9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "daa9365e-9e3e-4ee5-ac6a-bcad12177903",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}