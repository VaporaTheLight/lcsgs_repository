{
    "id": "77965914-7f05-4724-8ecf-d0a56ec3ef24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_center_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63dd2086-231e-44cf-8ec2-5bc989087273",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77965914-7f05-4724-8ecf-d0a56ec3ef24",
            "compositeImage": {
                "id": "6202a770-82b2-43c3-8dc8-466d81853735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63dd2086-231e-44cf-8ec2-5bc989087273",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2a15cf6-fc05-4c8a-874c-41a5d38530ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63dd2086-231e-44cf-8ec2-5bc989087273",
                    "LayerId": "718c3436-3688-4e5e-af34-dc57bddc8a05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "718c3436-3688-4e5e-af34-dc57bddc8a05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77965914-7f05-4724-8ecf-d0a56ec3ef24",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}