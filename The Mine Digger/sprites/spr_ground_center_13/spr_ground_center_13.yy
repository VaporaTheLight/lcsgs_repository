{
    "id": "aa62d59d-7208-4e3f-93cb-ee03845cfc37",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_center_13",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ab61f33-170d-4566-86c9-aa44d6f207ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa62d59d-7208-4e3f-93cb-ee03845cfc37",
            "compositeImage": {
                "id": "f943b9f2-e498-4292-ac38-a3db448f919f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ab61f33-170d-4566-86c9-aa44d6f207ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c44e297-3cc3-49a7-a44f-4df35082b6b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ab61f33-170d-4566-86c9-aa44d6f207ac",
                    "LayerId": "9475adab-e0f0-4e58-ba04-5e89bc7eace1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9475adab-e0f0-4e58-ba04-5e89bc7eace1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa62d59d-7208-4e3f-93cb-ee03845cfc37",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}