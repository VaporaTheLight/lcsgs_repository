{
    "id": "8161f16b-931c-465c-ae17-c0f004c18eb1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_down_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f632bad-8487-4ddb-b823-0ebdee7ee79d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8161f16b-931c-465c-ae17-c0f004c18eb1",
            "compositeImage": {
                "id": "55d85a83-4a24-4265-8f57-aa7d09525571",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f632bad-8487-4ddb-b823-0ebdee7ee79d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e26e4564-8478-48f5-b5a6-cb21c70ec665",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f632bad-8487-4ddb-b823-0ebdee7ee79d",
                    "LayerId": "49f581a0-e21b-4c84-b577-6d52b304516a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "49f581a0-e21b-4c84-b577-6d52b304516a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8161f16b-931c-465c-ae17-c0f004c18eb1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}