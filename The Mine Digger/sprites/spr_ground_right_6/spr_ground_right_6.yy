{
    "id": "c29b961b-52db-456a-9ce8-d06c3e79a729",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_right_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "027c28a2-5833-457a-81d5-b454f61bceaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c29b961b-52db-456a-9ce8-d06c3e79a729",
            "compositeImage": {
                "id": "f9e07f4d-2081-4eee-a08e-92139e7eb49d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "027c28a2-5833-457a-81d5-b454f61bceaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f7a7b6d-1e90-4efe-a378-dfd727748b25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "027c28a2-5833-457a-81d5-b454f61bceaf",
                    "LayerId": "ccf06736-5c6c-4427-9bd0-e0e642cbd11c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ccf06736-5c6c-4427-9bd0-e0e642cbd11c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c29b961b-52db-456a-9ce8-d06c3e79a729",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}