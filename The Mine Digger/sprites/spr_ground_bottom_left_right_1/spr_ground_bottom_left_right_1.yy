{
    "id": "8e94a866-56d1-4f43-932f-f3dced0f18d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_bottom_left_right_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15f70284-6e42-44fc-b24a-49424dfa77fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e94a866-56d1-4f43-932f-f3dced0f18d1",
            "compositeImage": {
                "id": "8130a385-ebcc-46e6-9a35-10f2f5b51932",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15f70284-6e42-44fc-b24a-49424dfa77fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b046b49-fb9c-47d6-8429-979e646edacd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15f70284-6e42-44fc-b24a-49424dfa77fa",
                    "LayerId": "0e1622de-040f-4d22-932e-5cfe5994626b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0e1622de-040f-4d22-932e-5cfe5994626b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e94a866-56d1-4f43-932f-f3dced0f18d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}