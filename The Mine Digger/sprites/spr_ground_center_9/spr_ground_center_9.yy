{
    "id": "ff0b37e4-df04-49cc-aa27-d216c1485081",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_center_9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c72695ae-8896-449b-b495-a57c6c567fab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff0b37e4-df04-49cc-aa27-d216c1485081",
            "compositeImage": {
                "id": "59a8829c-2a83-4497-8e03-4e3120f2d16f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c72695ae-8896-449b-b495-a57c6c567fab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32cb422a-3236-4cf8-8b69-056daae84d70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c72695ae-8896-449b-b495-a57c6c567fab",
                    "LayerId": "092ca629-6de1-4e47-90eb-3dcec653a083"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "092ca629-6de1-4e47-90eb-3dcec653a083",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff0b37e4-df04-49cc-aa27-d216c1485081",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}