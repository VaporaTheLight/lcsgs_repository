{
    "id": "12dac909-1313-4207-be0f-1f0bd2811f52",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_staircase",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "217486ff-2f99-4641-80a6-a514c4b52998",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12dac909-1313-4207-be0f-1f0bd2811f52",
            "compositeImage": {
                "id": "41c82e0c-68f5-4797-ab58-8bd22ce2a73d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "217486ff-2f99-4641-80a6-a514c4b52998",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90d1eb05-66ef-4b9a-aed3-1dd53288433a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "217486ff-2f99-4641-80a6-a514c4b52998",
                    "LayerId": "f1f9398a-e7e9-4f77-9a4a-93a0fe81a3c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f1f9398a-e7e9-4f77-9a4a-93a0fe81a3c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12dac909-1313-4207-be0f-1f0bd2811f52",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}