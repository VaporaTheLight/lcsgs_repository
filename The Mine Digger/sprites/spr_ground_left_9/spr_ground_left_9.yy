{
    "id": "6ecf3681-38e8-4bef-9c56-69f4393d668c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_left_9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5be382f4-66ed-4763-a489-b58ef8f9d006",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ecf3681-38e8-4bef-9c56-69f4393d668c",
            "compositeImage": {
                "id": "4e1e4ad2-8f3d-4ea1-a548-2209d1aed39d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5be382f4-66ed-4763-a489-b58ef8f9d006",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85816910-9b71-4984-bd09-14177bf493fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5be382f4-66ed-4763-a489-b58ef8f9d006",
                    "LayerId": "866dafc4-c4c0-415d-87ea-ced10a053a61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "866dafc4-c4c0-415d-87ea-ced10a053a61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ecf3681-38e8-4bef-9c56-69f4393d668c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}