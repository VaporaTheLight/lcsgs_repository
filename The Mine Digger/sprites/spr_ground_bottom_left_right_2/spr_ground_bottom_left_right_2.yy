{
    "id": "35a37eaa-c3c5-45ff-8c52-b4888983dc9d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_bottom_left_right_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a4ec8b5-f891-4def-9768-fb332a4768f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35a37eaa-c3c5-45ff-8c52-b4888983dc9d",
            "compositeImage": {
                "id": "9618a2b5-2fa2-4655-8978-b77c34e2cd75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a4ec8b5-f891-4def-9768-fb332a4768f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c411429-ba64-4ffd-9c0f-26cab0dfd9b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a4ec8b5-f891-4def-9768-fb332a4768f1",
                    "LayerId": "04a4f7fb-b8b8-44ae-93cb-13e8b37dfd2f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "04a4f7fb-b8b8-44ae-93cb-13e8b37dfd2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35a37eaa-c3c5-45ff-8c52-b4888983dc9d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}