{
    "id": "0e1ec8b9-de17-47a9-a748-bf6e1d875da2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bridge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "638de793-9766-4fbe-9429-eaa2f082214a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e1ec8b9-de17-47a9-a748-bf6e1d875da2",
            "compositeImage": {
                "id": "8a5840bc-98a0-4842-9b24-05e464ae1cb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "638de793-9766-4fbe-9429-eaa2f082214a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36c55873-f932-4222-a696-9f3647867c6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "638de793-9766-4fbe-9429-eaa2f082214a",
                    "LayerId": "1ecf92b6-2440-4ce3-a306-55baeb16cf7e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1ecf92b6-2440-4ce3-a306-55baeb16cf7e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e1ec8b9-de17-47a9-a748-bf6e1d875da2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}