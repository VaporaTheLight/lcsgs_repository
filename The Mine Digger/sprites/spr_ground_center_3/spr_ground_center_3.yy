{
    "id": "40e92725-1fb8-4cbe-8057-79b7ee612a54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_center_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6ba748c-0440-4f57-9224-5ac3ea9e683f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40e92725-1fb8-4cbe-8057-79b7ee612a54",
            "compositeImage": {
                "id": "efb4b8d7-d07e-4d7d-8851-9d517c50d6da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6ba748c-0440-4f57-9224-5ac3ea9e683f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02ab0f95-d550-4cea-a6d2-c03d060573ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6ba748c-0440-4f57-9224-5ac3ea9e683f",
                    "LayerId": "ea553006-eef0-47a2-aaeb-b5010b26de58"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ea553006-eef0-47a2-aaeb-b5010b26de58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40e92725-1fb8-4cbe-8057-79b7ee612a54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}