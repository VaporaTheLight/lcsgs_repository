{
    "id": "ff1cbb48-f086-40ca-b425-3f9ba7f4212b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_red_gem",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf52b98b-2eb1-46ec-aee0-87422e813e06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff1cbb48-f086-40ca-b425-3f9ba7f4212b",
            "compositeImage": {
                "id": "ce4cd535-ea5e-4c9c-9916-2e3fae3da977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf52b98b-2eb1-46ec-aee0-87422e813e06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdc2f6bf-4f0f-4a95-9404-1afa88ec5c94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf52b98b-2eb1-46ec-aee0-87422e813e06",
                    "LayerId": "c61bb1a4-71e9-488d-bb87-3cc16373d697"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c61bb1a4-71e9-488d-bb87-3cc16373d697",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff1cbb48-f086-40ca-b425-3f9ba7f4212b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}