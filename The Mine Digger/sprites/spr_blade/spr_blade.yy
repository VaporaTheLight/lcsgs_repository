{
    "id": "2a7a8295-abfe-4a7e-98e8-116b028324dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "757e9787-3a4f-4741-b06c-a0454c1b0c82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a7a8295-abfe-4a7e-98e8-116b028324dd",
            "compositeImage": {
                "id": "37be532c-5a1e-4a16-bc35-2dbe95eb5e83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "757e9787-3a4f-4741-b06c-a0454c1b0c82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8db1d53-893e-4ea7-afb6-96bd03294bc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "757e9787-3a4f-4741-b06c-a0454c1b0c82",
                    "LayerId": "b4dff7df-ec3c-4345-acb9-fa7d1c0ce5e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b4dff7df-ec3c-4345-acb9-fa7d1c0ce5e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a7a8295-abfe-4a7e-98e8-116b028324dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}