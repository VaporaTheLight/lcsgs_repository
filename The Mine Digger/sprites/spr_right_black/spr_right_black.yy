{
    "id": "f9f8c076-32fb-402c-aeb2-cb2f3fdc513a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_right_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6074dbe7-5ad7-4977-b195-4bc391799e0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9f8c076-32fb-402c-aeb2-cb2f3fdc513a",
            "compositeImage": {
                "id": "db5aca24-f702-4129-93d8-da059f2d415d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6074dbe7-5ad7-4977-b195-4bc391799e0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48513a8f-2213-487f-a44f-ad75023b95ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6074dbe7-5ad7-4977-b195-4bc391799e0d",
                    "LayerId": "42b1b36a-f362-4b4b-b5af-9638ffe13675"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "42b1b36a-f362-4b4b-b5af-9638ffe13675",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9f8c076-32fb-402c-aeb2-cb2f3fdc513a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}