{
    "id": "714a268e-913f-482e-a20e-80e8a73c28f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_up_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07ccbfa3-0849-459c-9bb5-72983fb103d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "714a268e-913f-482e-a20e-80e8a73c28f8",
            "compositeImage": {
                "id": "cd7560ef-d50b-4eb5-951b-9c1f743aa477",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07ccbfa3-0849-459c-9bb5-72983fb103d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcf74883-c906-418a-a502-933e293f0c63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07ccbfa3-0849-459c-9bb5-72983fb103d7",
                    "LayerId": "032d0aeb-0223-4879-94d7-69bed8c0894a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "032d0aeb-0223-4879-94d7-69bed8c0894a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "714a268e-913f-482e-a20e-80e8a73c28f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}