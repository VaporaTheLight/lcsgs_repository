{
    "id": "16bcf9e4-4391-4c97-8cee-0ac432a59207",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ebcf4a9-457e-43f6-bce8-20d907f48bb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16bcf9e4-4391-4c97-8cee-0ac432a59207",
            "compositeImage": {
                "id": "27721f7d-9512-4ee2-a5ff-1b1d361186f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ebcf4a9-457e-43f6-bce8-20d907f48bb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83adc548-b487-4174-ae7b-911299414ed8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ebcf4a9-457e-43f6-bce8-20d907f48bb1",
                    "LayerId": "dc1091f7-58fd-4b48-94b5-1a6e419a3ac5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dc1091f7-58fd-4b48-94b5-1a6e419a3ac5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16bcf9e4-4391-4c97-8cee-0ac432a59207",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}