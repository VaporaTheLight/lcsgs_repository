{
    "id": "268935c7-0cac-423e-b4da-65d23d829a54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sstartBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff44fc92-3bb3-4434-9734-5ce9ea302892",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "268935c7-0cac-423e-b4da-65d23d829a54",
            "compositeImage": {
                "id": "b232b57b-9c83-4e59-af61-191f7d02b3ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff44fc92-3bb3-4434-9734-5ce9ea302892",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "017fd3a0-56a9-41da-a184-3eb6dce9ae21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff44fc92-3bb3-4434-9734-5ce9ea302892",
                    "LayerId": "0d6d7ce7-e7ef-459d-bacc-82d1a5d707ef"
                }
            ]
        },
        {
            "id": "11bb3521-9064-4a2a-b649-3cd67baddbe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "268935c7-0cac-423e-b4da-65d23d829a54",
            "compositeImage": {
                "id": "2fd7252d-0087-4398-8786-c7e50187c1b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11bb3521-9064-4a2a-b649-3cd67baddbe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83fd71ed-bd76-4625-af10-121ea5e37c8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11bb3521-9064-4a2a-b649-3cd67baddbe5",
                    "LayerId": "0d6d7ce7-e7ef-459d-bacc-82d1a5d707ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "0d6d7ce7-e7ef-459d-bacc-82d1a5d707ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "268935c7-0cac-423e-b4da-65d23d829a54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}