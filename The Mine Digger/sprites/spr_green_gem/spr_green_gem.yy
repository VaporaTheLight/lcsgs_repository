{
    "id": "82d66b99-b463-4af4-b382-0c5c8a20ac90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_green_gem",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "972def7a-80bd-4de9-bd3b-9668a3279355",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82d66b99-b463-4af4-b382-0c5c8a20ac90",
            "compositeImage": {
                "id": "a49d3c6b-c1d9-41a6-b5ad-2ea5c209123e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "972def7a-80bd-4de9-bd3b-9668a3279355",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4d26710-bc5e-49a1-9b5f-e7e7abce3399",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "972def7a-80bd-4de9-bd3b-9668a3279355",
                    "LayerId": "b97dd589-8ee8-44c6-ba7a-005b47c74879"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b97dd589-8ee8-44c6-ba7a-005b47c74879",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82d66b99-b463-4af4-b382-0c5c8a20ac90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}