{
    "id": "b30d78a9-25bb-448e-a233-471e586ca043",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_center_12",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "945c23ff-f290-4056-81cc-e3f3ffd99fd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b30d78a9-25bb-448e-a233-471e586ca043",
            "compositeImage": {
                "id": "01fb42de-7942-42b1-88b0-3016efe56cdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "945c23ff-f290-4056-81cc-e3f3ffd99fd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0413535e-2b77-4eb1-b310-2369b46d8114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "945c23ff-f290-4056-81cc-e3f3ffd99fd6",
                    "LayerId": "b205d6e4-e93d-4a10-bfdc-fdddfc728125"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b205d6e4-e93d-4a10-bfdc-fdddfc728125",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b30d78a9-25bb-448e-a233-471e586ca043",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}