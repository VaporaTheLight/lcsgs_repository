{
    "id": "df7bec13-1c3b-4e5a-a11e-da1cebb62a10",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_left_11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1f51926-77c0-4f6d-b807-1aa285ba9376",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df7bec13-1c3b-4e5a-a11e-da1cebb62a10",
            "compositeImage": {
                "id": "536d37b5-8608-4acd-982e-a9f7191a6a6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1f51926-77c0-4f6d-b807-1aa285ba9376",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0fe55ca-c283-464c-aebe-cdc660c4e01f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1f51926-77c0-4f6d-b807-1aa285ba9376",
                    "LayerId": "4b28f715-20e8-4474-8559-a015019d4500"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4b28f715-20e8-4474-8559-a015019d4500",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df7bec13-1c3b-4e5a-a11e-da1cebb62a10",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}