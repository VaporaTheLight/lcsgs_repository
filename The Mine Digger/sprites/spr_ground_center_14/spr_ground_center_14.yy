{
    "id": "ae4fb2ce-5812-4f88-afcb-8c8f8752a73f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_center_14",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea25e022-7178-47d6-b4f6-b6dd90cf455d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae4fb2ce-5812-4f88-afcb-8c8f8752a73f",
            "compositeImage": {
                "id": "585746ad-31e4-4d1c-add5-2ecb621d7550",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea25e022-7178-47d6-b4f6-b6dd90cf455d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "788d840a-07b7-4e66-9572-8b0d0b3be65e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea25e022-7178-47d6-b4f6-b6dd90cf455d",
                    "LayerId": "6d9da13c-9274-4fc9-96ec-f94bf800d2be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6d9da13c-9274-4fc9-96ec-f94bf800d2be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae4fb2ce-5812-4f88-afcb-8c8f8752a73f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}