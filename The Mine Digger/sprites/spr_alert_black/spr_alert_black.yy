{
    "id": "4baf618c-f532-46af-8fd2-b52e7bc68074",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_alert_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3a64c3b-9aa9-4e16-b4b8-485940b95cc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4baf618c-f532-46af-8fd2-b52e7bc68074",
            "compositeImage": {
                "id": "5317ea09-2d1a-4581-9afc-a46b7406ed3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3a64c3b-9aa9-4e16-b4b8-485940b95cc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e32ae8b-631d-432c-ade3-fa0c813e13bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3a64c3b-9aa9-4e16-b4b8-485940b95cc7",
                    "LayerId": "dd276cbc-2a0e-4842-a491-d95375a56d03"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "dd276cbc-2a0e-4842-a491-d95375a56d03",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4baf618c-f532-46af-8fd2-b52e7bc68074",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}