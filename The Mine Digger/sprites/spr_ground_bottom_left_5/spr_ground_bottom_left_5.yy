{
    "id": "b9e17aa7-00a2-4a03-aa66-0551dd73b4ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_bottom_left_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37a02100-d3b0-43ce-b31c-bda9910c2edc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9e17aa7-00a2-4a03-aa66-0551dd73b4ad",
            "compositeImage": {
                "id": "5aeb3fd8-365f-4dca-8e1c-af50c61c825e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37a02100-d3b0-43ce-b31c-bda9910c2edc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f291a922-e826-46b1-b532-22f5e53c9634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37a02100-d3b0-43ce-b31c-bda9910c2edc",
                    "LayerId": "261fbffb-1afd-4c58-9d0a-39f46180515c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "261fbffb-1afd-4c58-9d0a-39f46180515c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9e17aa7-00a2-4a03-aa66-0551dd73b4ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}