{
    "id": "0609fa67-35d5-4be9-b8c2-b45ad169ae9d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_miner_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "109de4b3-1417-49d5-84a6-d9551ff0fbdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0609fa67-35d5-4be9-b8c2-b45ad169ae9d",
            "compositeImage": {
                "id": "f9740d3a-7b5f-41ee-8365-74b1299c7677",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "109de4b3-1417-49d5-84a6-d9551ff0fbdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef2ece27-87c4-4207-866d-aa544f0ff76b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "109de4b3-1417-49d5-84a6-d9551ff0fbdd",
                    "LayerId": "16b61112-f4e6-47b5-b8f0-9f86bf024392"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "16b61112-f4e6-47b5-b8f0-9f86bf024392",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0609fa67-35d5-4be9-b8c2-b45ad169ae9d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}