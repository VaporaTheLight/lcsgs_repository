{
    "id": "445364ff-8156-4393-b5d9-3e3616fb238d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_right_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d246f029-a239-4a02-b168-73eb0ed25c7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "445364ff-8156-4393-b5d9-3e3616fb238d",
            "compositeImage": {
                "id": "ce76b6f6-51d0-445d-a3a0-dbdb6ade68a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d246f029-a239-4a02-b168-73eb0ed25c7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "666c4ba4-db07-43a7-a7e7-716eb6dc4944",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d246f029-a239-4a02-b168-73eb0ed25c7c",
                    "LayerId": "22f52ba8-ac6b-4a02-85eb-e81615342ee3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "22f52ba8-ac6b-4a02-85eb-e81615342ee3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "445364ff-8156-4393-b5d9-3e3616fb238d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}