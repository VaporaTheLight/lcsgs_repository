{
    "id": "5c209b01-d55f-4fb7-82db-bf20c465b947",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_center_17",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad7291b2-ddb4-4977-885d-84dc438cb757",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c209b01-d55f-4fb7-82db-bf20c465b947",
            "compositeImage": {
                "id": "bbb1f5e4-ad53-42dd-b158-602da5d29d27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad7291b2-ddb4-4977-885d-84dc438cb757",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c77ac77b-7dc2-40d2-b3dd-1b3e5cc5d8d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad7291b2-ddb4-4977-885d-84dc438cb757",
                    "LayerId": "44b8c509-fba1-4ec9-b92f-0c66174e1d34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "44b8c509-fba1-4ec9-b92f-0c66174e1d34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c209b01-d55f-4fb7-82db-bf20c465b947",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}