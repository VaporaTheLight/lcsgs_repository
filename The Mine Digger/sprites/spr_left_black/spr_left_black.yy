{
    "id": "96f8a761-979e-4030-adfe-f5fdf4b01701",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_left_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "691032a4-69eb-4274-b954-1c117c24ab5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f8a761-979e-4030-adfe-f5fdf4b01701",
            "compositeImage": {
                "id": "1edd581a-c537-42ba-a147-5dbee29931a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "691032a4-69eb-4274-b954-1c117c24ab5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20b2cbe8-cf4a-4689-a5c2-c4bc15976cbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "691032a4-69eb-4274-b954-1c117c24ab5b",
                    "LayerId": "9d7a8ffe-1013-43e0-9535-c071b7eb3296"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9d7a8ffe-1013-43e0-9535-c071b7eb3296",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96f8a761-979e-4030-adfe-f5fdf4b01701",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}