{
    "id": "cfcc1a84-c91e-4102-9147-538cebb62e19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_thorns",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4bc6666-c7e0-42a5-b975-5a94dccbbda5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfcc1a84-c91e-4102-9147-538cebb62e19",
            "compositeImage": {
                "id": "87e56042-d2a6-4a0f-abaf-ac3015751e13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4bc6666-c7e0-42a5-b975-5a94dccbbda5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75bd53ed-48e7-4e75-9642-5ebe661533f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4bc6666-c7e0-42a5-b975-5a94dccbbda5",
                    "LayerId": "86eef1ac-2a24-4efb-a5e2-37960c60a8ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "86eef1ac-2a24-4efb-a5e2-37960c60a8ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cfcc1a84-c91e-4102-9147-538cebb62e19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}