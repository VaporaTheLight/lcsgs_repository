{
    "id": "2de88793-16f5-4e04-9c34-fecbb25ab83b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oStartBar",
    "eventList": [
        {
            "id": "2f03f535-87d8-4340-8b70-348c5cae3e81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2de88793-16f5-4e04-9c34-fecbb25ab83b"
        },
        {
            "id": "feae8768-357b-475f-ad43-d5525e023796",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "2de88793-16f5-4e04-9c34-fecbb25ab83b"
        },
        {
            "id": "b490a1cc-2878-4873-a046-a611ccce7999",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "2de88793-16f5-4e04-9c34-fecbb25ab83b"
        },
        {
            "id": "1f890ae6-174f-49dd-ac94-a6f610fe272a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "2de88793-16f5-4e04-9c34-fecbb25ab83b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "268935c7-0cac-423e-b4da-65d23d829a54",
    "visible": true
}