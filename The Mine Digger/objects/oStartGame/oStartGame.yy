{
    "id": "33d4090f-cc40-4897-9382-2e4b81740c71",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oStartGame",
    "eventList": [
        {
            "id": "4ac16ac0-43ef-426c-a32e-c1d0d8a5b00e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "33d4090f-cc40-4897-9382-2e4b81740c71"
        },
        {
            "id": "1f8bae54-75ec-4fe8-a2c3-588497221ef7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "33d4090f-cc40-4897-9382-2e4b81740c71"
        },
        {
            "id": "eaa5e460-b93f-4c32-a46e-96557d3734eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "33d4090f-cc40-4897-9382-2e4b81740c71"
        },
        {
            "id": "b5fda7bf-21b6-417b-8f8a-4066c8461d20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "33d4090f-cc40-4897-9382-2e4b81740c71"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}