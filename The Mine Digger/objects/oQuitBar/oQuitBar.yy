{
    "id": "812ba986-3b20-4adb-9180-5bd3bc9cecd5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oQuitBar",
    "eventList": [
        {
            "id": "34213a90-27f8-4e5b-86a4-1e0541735089",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "812ba986-3b20-4adb-9180-5bd3bc9cecd5"
        },
        {
            "id": "770fe57c-5d66-400c-901c-e4fdd0837d65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "812ba986-3b20-4adb-9180-5bd3bc9cecd5"
        },
        {
            "id": "7f68da58-3734-41a6-95d7-a5f6b86cfedf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "812ba986-3b20-4adb-9180-5bd3bc9cecd5"
        },
        {
            "id": "5154309c-716c-4181-8397-06499ca167f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "812ba986-3b20-4adb-9180-5bd3bc9cecd5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aeed01c4-c86b-43b6-a02c-3d8bccc1b963",
    "visible": true
}