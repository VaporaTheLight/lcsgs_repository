{
    "id": "c0b1ad7d-bba8-4d33-99cb-0758b6f0c139",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "2e4cc3d0-8136-497a-85ba-e150d8d91d3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c0b1ad7d-bba8-4d33-99cb-0758b6f0c139"
        },
        {
            "id": "bd766271-77dc-44ac-9426-8794bbff595d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c0b1ad7d-bba8-4d33-99cb-0758b6f0c139"
        },
        {
            "id": "902fe657-8b7b-486e-a899-c0458cf12118",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3e4d874a-14fc-4519-a72c-75ada839b23c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c0b1ad7d-bba8-4d33-99cb-0758b6f0c139"
        },
        {
            "id": "a2e6ba3f-6b07-4821-b5ae-6625fd099809",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0f4ec57f-61fa-4802-95c2-ca29bb02ca55",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c0b1ad7d-bba8-4d33-99cb-0758b6f0c139"
        },
        {
            "id": "4c35f550-9d24-4ef5-9ec7-3e2657a5ca7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "c0b1ad7d-bba8-4d33-99cb-0758b6f0c139"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "19948cdf-a151-4684-894b-c135f2ec0583",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "variable_name",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "a3faf60a-bb08-481c-92e1-c3d3cb367a63",
    "visible": true
}