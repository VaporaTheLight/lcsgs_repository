{
    "id": "bbc0ea79-7bd6-433c-8dd8-9828ba2f3b08",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oQuitGame",
    "eventList": [
        {
            "id": "8b847933-b23c-40f0-a27a-41794c99a146",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bbc0ea79-7bd6-433c-8dd8-9828ba2f3b08"
        },
        {
            "id": "a782cb2e-cd6d-4e59-9381-c3f96d839b32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "bbc0ea79-7bd6-433c-8dd8-9828ba2f3b08"
        },
        {
            "id": "aeb7ac20-f927-4dd6-874d-e7ad4da10d69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "bbc0ea79-7bd6-433c-8dd8-9828ba2f3b08"
        },
        {
            "id": "ac55b412-355e-43d6-96e4-7d3ba2bc317f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "bbc0ea79-7bd6-433c-8dd8-9828ba2f3b08"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}