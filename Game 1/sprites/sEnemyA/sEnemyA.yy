{
    "id": "2a06d5f4-d8df-4c2d-9269-d7d869c64564",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "df3f1cee-aafb-4946-b9d9-b9a87409bc2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a06d5f4-d8df-4c2d-9269-d7d869c64564",
            "compositeImage": {
                "id": "3687a608-f62c-473d-a714-aa2f243f5b80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df3f1cee-aafb-4946-b9d9-b9a87409bc2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faac7884-eca4-4308-ba35-5bdc1e948565",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df3f1cee-aafb-4946-b9d9-b9a87409bc2d",
                    "LayerId": "9a0195cb-bbc5-4220-8d7b-007a38586223"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9a0195cb-bbc5-4220-8d7b-007a38586223",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a06d5f4-d8df-4c2d-9269-d7d869c64564",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}