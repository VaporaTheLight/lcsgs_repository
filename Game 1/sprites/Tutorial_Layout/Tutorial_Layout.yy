{
    "id": "c33f0bf9-4a15-4297-a610-b76a652aaf76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Tutorial_Layout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2159,
    "bbox_left": 0,
    "bbox_right": 4319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fce0f054-7fb1-485e-a012-c4b4ad6d8e4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c33f0bf9-4a15-4297-a610-b76a652aaf76",
            "compositeImage": {
                "id": "77b8d528-94fc-4e5d-a653-111e5dfa0fcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fce0f054-7fb1-485e-a012-c4b4ad6d8e4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8011e9c5-8018-43c1-8746-5a9cc3c2c713",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fce0f054-7fb1-485e-a012-c4b4ad6d8e4e",
                    "LayerId": "59bd8b0c-d5b8-4489-a023-ba5da5d5a142"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2160,
    "layers": [
        {
            "id": "59bd8b0c-d5b8-4489-a023-ba5da5d5a142",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c33f0bf9-4a15-4297-a610-b76a652aaf76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4320,
    "xorig": 0,
    "yorig": 0
}