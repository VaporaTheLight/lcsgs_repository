{
    "id": "7ef6968d-6bf0-41ae-bc10-65f4c9d18ce8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSewer_Tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26178428-9287-4506-a51a-877d0a90ce41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ef6968d-6bf0-41ae-bc10-65f4c9d18ce8",
            "compositeImage": {
                "id": "7e6fbb09-bc15-4849-bf36-e1157d0c498d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26178428-9287-4506-a51a-877d0a90ce41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2257b0f6-46c1-4962-b311-e59a6704caab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26178428-9287-4506-a51a-877d0a90ce41",
                    "LayerId": "d613dacf-3f91-4aad-9d85-95d1d10858a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d613dacf-3f91-4aad-9d85-95d1d10858a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ef6968d-6bf0-41ae-bc10-65f4c9d18ce8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}