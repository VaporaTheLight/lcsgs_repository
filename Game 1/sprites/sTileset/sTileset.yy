{
    "id": "7ef6968d-6bf0-41ae-bc10-65f4c9d18ce8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e854ea91-527c-42b3-9b64-164ab4dbe29b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ef6968d-6bf0-41ae-bc10-65f4c9d18ce8",
            "compositeImage": {
                "id": "ff8218de-735f-4c38-abca-feee2921a093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e854ea91-527c-42b3-9b64-164ab4dbe29b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21e68d9c-6e7f-4164-88e3-afa4059c66e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e854ea91-527c-42b3-9b64-164ab4dbe29b",
                    "LayerId": "91dafef4-fd2f-4a4b-9477-9429caee3465"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "91dafef4-fd2f-4a4b-9477-9429caee3465",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ef6968d-6bf0-41ae-bc10-65f4c9d18ce8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}