{
    "id": "0fa378a8-beea-46ef-9b78-3ebca9ba7c25",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7db16bcb-933e-4109-beee-1db35551798d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fa378a8-beea-46ef-9b78-3ebca9ba7c25",
            "compositeImage": {
                "id": "12ef2e23-d380-4d84-ad98-6af519bed02e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7db16bcb-933e-4109-beee-1db35551798d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0741612a-0aa3-4fc2-bea4-dd49b2c23db5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7db16bcb-933e-4109-beee-1db35551798d",
                    "LayerId": "c899847e-fb43-4d8f-ab46-e700799c84d8"
                }
            ]
        },
        {
            "id": "f8ee35f1-e45a-4131-a7ac-938b0d42bb28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fa378a8-beea-46ef-9b78-3ebca9ba7c25",
            "compositeImage": {
                "id": "0978db2e-186b-43aa-a628-59c969e48a37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8ee35f1-e45a-4131-a7ac-938b0d42bb28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1c09ed4-4e99-4c7b-a360-ddae93c0586f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8ee35f1-e45a-4131-a7ac-938b0d42bb28",
                    "LayerId": "c899847e-fb43-4d8f-ab46-e700799c84d8"
                }
            ]
        },
        {
            "id": "64889f6c-76da-49ee-a414-25bdd47056ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fa378a8-beea-46ef-9b78-3ebca9ba7c25",
            "compositeImage": {
                "id": "b4383989-0cce-436e-9c87-00b423e43651",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64889f6c-76da-49ee-a414-25bdd47056ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ca6f6a9-3ef6-4246-80d6-f19197550679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64889f6c-76da-49ee-a414-25bdd47056ce",
                    "LayerId": "c899847e-fb43-4d8f-ab46-e700799c84d8"
                }
            ]
        },
        {
            "id": "74c84d45-e995-42e3-b245-28b9e4a2ee56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fa378a8-beea-46ef-9b78-3ebca9ba7c25",
            "compositeImage": {
                "id": "959c2ecf-4653-4469-ad99-f85c4f26a821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74c84d45-e995-42e3-b245-28b9e4a2ee56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f693f8e-588d-423e-a945-b9debd853706",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74c84d45-e995-42e3-b245-28b9e4a2ee56",
                    "LayerId": "c899847e-fb43-4d8f-ab46-e700799c84d8"
                }
            ]
        },
        {
            "id": "b9c35315-4c5c-4c8f-ba61-1747c005ca55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fa378a8-beea-46ef-9b78-3ebca9ba7c25",
            "compositeImage": {
                "id": "2a7b25aa-e309-4b86-b5a6-c70175288858",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9c35315-4c5c-4c8f-ba61-1747c005ca55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10ea6dc5-a4b2-4d68-b728-1c230c6a60ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9c35315-4c5c-4c8f-ba61-1747c005ca55",
                    "LayerId": "c899847e-fb43-4d8f-ab46-e700799c84d8"
                }
            ]
        },
        {
            "id": "2903b8a2-970e-4fdc-be81-7bafefce5462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fa378a8-beea-46ef-9b78-3ebca9ba7c25",
            "compositeImage": {
                "id": "6e07fb75-c422-448e-9785-677220a8274b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2903b8a2-970e-4fdc-be81-7bafefce5462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "244642d5-4d4a-45d9-9fcf-c4a584546d83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2903b8a2-970e-4fdc-be81-7bafefce5462",
                    "LayerId": "c899847e-fb43-4d8f-ab46-e700799c84d8"
                }
            ]
        },
        {
            "id": "043e6923-5b86-4ce1-b9b9-6185f111a224",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fa378a8-beea-46ef-9b78-3ebca9ba7c25",
            "compositeImage": {
                "id": "71e05783-82a8-4f83-a12a-e16902ea2ef2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "043e6923-5b86-4ce1-b9b9-6185f111a224",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a14e0737-0315-4ff2-b576-952ac4688818",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "043e6923-5b86-4ce1-b9b9-6185f111a224",
                    "LayerId": "c899847e-fb43-4d8f-ab46-e700799c84d8"
                }
            ]
        },
        {
            "id": "cf78765a-2a0c-447e-851c-b4cc86429ffa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fa378a8-beea-46ef-9b78-3ebca9ba7c25",
            "compositeImage": {
                "id": "718c9f7f-12a0-492f-996e-2c7b7b3da7ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf78765a-2a0c-447e-851c-b4cc86429ffa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e04a74d6-adfc-4366-91c1-3aed1fdce0f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf78765a-2a0c-447e-851c-b4cc86429ffa",
                    "LayerId": "c899847e-fb43-4d8f-ab46-e700799c84d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "c899847e-fb43-4d8f-ab46-e700799c84d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0fa378a8-beea-46ef-9b78-3ebca9ba7c25",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 0,
    "yorig": 0
}