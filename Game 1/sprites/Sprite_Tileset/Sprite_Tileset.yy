{
    "id": "7152cfc2-a218-493d-b6d6-c3e29c116cab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Sprite_Tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c70648a-0907-4253-a1a8-1dabe2545357",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7152cfc2-a218-493d-b6d6-c3e29c116cab",
            "compositeImage": {
                "id": "b85d03df-e114-4b16-9378-23c5b0faa386",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c70648a-0907-4253-a1a8-1dabe2545357",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96e38f15-f9f2-46e1-bee0-80232db4210d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c70648a-0907-4253-a1a8-1dabe2545357",
                    "LayerId": "4681d2f6-b787-4c5f-b99d-0f0c10926395"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "4681d2f6-b787-4c5f-b99d-0f0c10926395",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7152cfc2-a218-493d-b6d6-c3e29c116cab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}