{
    "id": "bbd75ed3-be51-4907-9e56-3b0a6eb6c586",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHPBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "090f64fe-e6c9-422a-a7a3-7239bbbe0dee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbd75ed3-be51-4907-9e56-3b0a6eb6c586",
            "compositeImage": {
                "id": "36670924-b84d-4192-ad66-1f2393d99158",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "090f64fe-e6c9-422a-a7a3-7239bbbe0dee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4088da27-9872-49c1-aedd-54a9929dd2dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "090f64fe-e6c9-422a-a7a3-7239bbbe0dee",
                    "LayerId": "ee49020e-d540-42b3-b6ec-739a451a963f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ee49020e-d540-42b3-b6ec-739a451a963f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbd75ed3-be51-4907-9e56-3b0a6eb6c586",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 8
}