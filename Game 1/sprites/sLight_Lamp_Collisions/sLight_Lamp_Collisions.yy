{
    "id": "4275ce5e-157a-41a0-befd-3b05bef93449",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLight_Lamp_Collisions",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f90f2dc-39e5-44c8-a778-b646861ff03f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4275ce5e-157a-41a0-befd-3b05bef93449",
            "compositeImage": {
                "id": "db2837d3-a475-4e4d-a0f6-9af2d17d17fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f90f2dc-39e5-44c8-a778-b646861ff03f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79cc7a65-6aa5-4bc4-bd44-910b87eaed13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f90f2dc-39e5-44c8-a778-b646861ff03f",
                    "LayerId": "71235298-69c5-4686-b53c-9658534d3a83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "71235298-69c5-4686-b53c-9658534d3a83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4275ce5e-157a-41a0-befd-3b05bef93449",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}