{
    "id": "6aaf3b4f-5927-4156-89bd-c07ce48d4ff5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 3,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65fe3d6d-5367-4247-9045-e2d30e81288e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aaf3b4f-5927-4156-89bd-c07ce48d4ff5",
            "compositeImage": {
                "id": "b682794d-3d74-471f-9acf-6f2763d645b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65fe3d6d-5367-4247-9045-e2d30e81288e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "805124d6-bbb6-43d9-b72e-c2dd668b35cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65fe3d6d-5367-4247-9045-e2d30e81288e",
                    "LayerId": "c28f00fa-1aea-488a-904e-66a969476de1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "c28f00fa-1aea-488a-904e-66a969476de1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6aaf3b4f-5927-4156-89bd-c07ce48d4ff5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 0,
    "yorig": 0
}