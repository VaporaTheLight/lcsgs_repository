{
    "id": "858e2b5d-90d8-4360-86b9-95ca7b9bf88a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDust",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8ddd43c-4704-4dee-9e26-c054894858ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "858e2b5d-90d8-4360-86b9-95ca7b9bf88a",
            "compositeImage": {
                "id": "3e74e018-c63a-469a-a241-788e86d0bfd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8ddd43c-4704-4dee-9e26-c054894858ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcd8f411-8722-4bc2-b149-72efb71927d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8ddd43c-4704-4dee-9e26-c054894858ea",
                    "LayerId": "d08503c3-5156-403a-a714-cc40416ad0e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "d08503c3-5156-403a-a714-cc40416ad0e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "858e2b5d-90d8-4360-86b9-95ca7b9bf88a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}