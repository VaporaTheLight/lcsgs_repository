{
    "id": "bbf390c9-58cb-4b2b-b214-4e08eac1e8a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEneemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17622b22-499a-48dd-a8fa-291a36f93d4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbf390c9-58cb-4b2b-b214-4e08eac1e8a1",
            "compositeImage": {
                "id": "ef0ed698-713b-4006-8e96-e3575e365de6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17622b22-499a-48dd-a8fa-291a36f93d4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad7bd3e0-90bc-43c2-a2ae-30d8ecb658a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17622b22-499a-48dd-a8fa-291a36f93d4b",
                    "LayerId": "ec69a6dc-909a-45f6-86a1-9c476982140a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ec69a6dc-909a-45f6-86a1-9c476982140a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbf390c9-58cb-4b2b-b214-4e08eac1e8a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}