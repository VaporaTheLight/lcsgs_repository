{
    "id": "21a18e37-df8d-4b24-af17-fcc32aa10e7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDashplayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 12,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "32c863ac-2cfa-4a4a-88ec-658fe4ef4405",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21a18e37-df8d-4b24-af17-fcc32aa10e7c",
            "compositeImage": {
                "id": "038e793a-fe7e-4b82-886a-ac4a2e8b1bd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32c863ac-2cfa-4a4a-88ec-658fe4ef4405",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0590d05-647e-49ff-ad1c-f9edd050f1f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c863ac-2cfa-4a4a-88ec-658fe4ef4405",
                    "LayerId": "e071e1f6-7705-49f2-a74f-c2240586be55"
                }
            ]
        },
        {
            "id": "9eef90d8-50f1-4e99-b3d1-7bbf6745b636",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21a18e37-df8d-4b24-af17-fcc32aa10e7c",
            "compositeImage": {
                "id": "624a8a2d-4871-4e7c-a52a-5edfece87281",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eef90d8-50f1-4e99-b3d1-7bbf6745b636",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce2b30af-1f4d-4d15-8352-56bb71095c3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eef90d8-50f1-4e99-b3d1-7bbf6745b636",
                    "LayerId": "e071e1f6-7705-49f2-a74f-c2240586be55"
                }
            ]
        },
        {
            "id": "f14c097b-b0e8-45d1-8069-a38b6348955b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21a18e37-df8d-4b24-af17-fcc32aa10e7c",
            "compositeImage": {
                "id": "0a923d86-7cee-46a3-8a75-9c597e2977cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f14c097b-b0e8-45d1-8069-a38b6348955b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a000e48-1506-4dd0-9261-47912388afc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f14c097b-b0e8-45d1-8069-a38b6348955b",
                    "LayerId": "e071e1f6-7705-49f2-a74f-c2240586be55"
                }
            ]
        },
        {
            "id": "53f609b8-4f8b-413d-b0ef-0ab7bcb936f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21a18e37-df8d-4b24-af17-fcc32aa10e7c",
            "compositeImage": {
                "id": "51e85f45-5502-4b45-aa7b-88704a27a147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53f609b8-4f8b-413d-b0ef-0ab7bcb936f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "307a8382-941d-4224-b954-67c7a3cd9b42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53f609b8-4f8b-413d-b0ef-0ab7bcb936f8",
                    "LayerId": "e071e1f6-7705-49f2-a74f-c2240586be55"
                }
            ]
        },
        {
            "id": "c276f653-fcd1-4753-9006-8b77dac557d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21a18e37-df8d-4b24-af17-fcc32aa10e7c",
            "compositeImage": {
                "id": "fb740d35-2071-44c8-ac5d-9243abd83a51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c276f653-fcd1-4753-9006-8b77dac557d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04397cc4-c501-41e3-aea6-060c62b43256",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c276f653-fcd1-4753-9006-8b77dac557d8",
                    "LayerId": "e071e1f6-7705-49f2-a74f-c2240586be55"
                }
            ]
        },
        {
            "id": "0b062a02-18f8-4c77-a1f5-ad839e0693c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21a18e37-df8d-4b24-af17-fcc32aa10e7c",
            "compositeImage": {
                "id": "025bb35d-411a-42f4-8d93-411d7da3cae5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b062a02-18f8-4c77-a1f5-ad839e0693c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "196b47a3-20bf-4cda-ab1c-2a457839f890",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b062a02-18f8-4c77-a1f5-ad839e0693c1",
                    "LayerId": "e071e1f6-7705-49f2-a74f-c2240586be55"
                }
            ]
        },
        {
            "id": "d489bb9a-482a-432c-a40f-22d9f296fbdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21a18e37-df8d-4b24-af17-fcc32aa10e7c",
            "compositeImage": {
                "id": "8afa18e4-63cf-479c-96ad-e636dec5e4e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d489bb9a-482a-432c-a40f-22d9f296fbdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbca4ce0-b57b-4a20-80cf-6fe39b029ee7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d489bb9a-482a-432c-a40f-22d9f296fbdd",
                    "LayerId": "e071e1f6-7705-49f2-a74f-c2240586be55"
                }
            ]
        },
        {
            "id": "eb691326-33e9-46be-942f-cb0ddb9c0a6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21a18e37-df8d-4b24-af17-fcc32aa10e7c",
            "compositeImage": {
                "id": "f43af3a8-bda1-4b36-9b58-9f9af15b1270",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb691326-33e9-46be-942f-cb0ddb9c0a6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "651d56d7-5ca3-40b2-9eb2-c216ae949576",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb691326-33e9-46be-942f-cb0ddb9c0a6d",
                    "LayerId": "e071e1f6-7705-49f2-a74f-c2240586be55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "e071e1f6-7705-49f2-a74f-c2240586be55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21a18e37-df8d-4b24-af17-fcc32aa10e7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 0,
    "yorig": 0
}