{
    "id": "87b24ffa-f73f-4b43-961f-f713f82d4948",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sItem",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4b47f3b-60e6-4045-b2d1-4efd2c623dfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87b24ffa-f73f-4b43-961f-f713f82d4948",
            "compositeImage": {
                "id": "42c33d92-4347-4461-8865-0528cd3783c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4b47f3b-60e6-4045-b2d1-4efd2c623dfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb0da91f-b7b3-44bf-b1e8-3ec1a41b7689",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4b47f3b-60e6-4045-b2d1-4efd2c623dfb",
                    "LayerId": "bd0011ff-4862-4d1f-8636-ffaacbdcae15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bd0011ff-4862-4d1f-8636-ffaacbdcae15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87b24ffa-f73f-4b43-961f-f713f82d4948",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}