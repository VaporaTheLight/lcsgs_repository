{
    "id": "f1ab99f5-c1c3-40fe-9fb3-3e46c2365061",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sInvWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c082239e-aeb5-4078-95c9-365d691d6abc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1ab99f5-c1c3-40fe-9fb3-3e46c2365061",
            "compositeImage": {
                "id": "d8b5e4da-84a8-4a5e-b87c-275451715399",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c082239e-aeb5-4078-95c9-365d691d6abc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a93f9a6-b4f7-42f1-9c09-36af6f5104a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c082239e-aeb5-4078-95c9-365d691d6abc",
                    "LayerId": "c6887a4d-94ee-4122-9297-49ee02709242"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c6887a4d-94ee-4122-9297-49ee02709242",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1ab99f5-c1c3-40fe-9fb3-3e46c2365061",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}