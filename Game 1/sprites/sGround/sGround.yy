{
    "id": "1906338b-9e68-43bb-b6c5-6bca781111b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGround",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a96df5b-785e-4d7f-9e07-cd6e5ac13866",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1906338b-9e68-43bb-b6c5-6bca781111b6",
            "compositeImage": {
                "id": "5aa1dabd-7c68-4d09-97cd-e66ee486587b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a96df5b-785e-4d7f-9e07-cd6e5ac13866",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5ca1970-a7a2-4ce4-ac7e-f4a8b0aa9834",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a96df5b-785e-4d7f-9e07-cd6e5ac13866",
                    "LayerId": "01d7b29c-02f3-44f7-bd81-aa8c1d01b731"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "01d7b29c-02f3-44f7-bd81-aa8c1d01b731",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1906338b-9e68-43bb-b6c5-6bca781111b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}