{
    "id": "6c6ae428-a5f9-40e6-bf9a-a2381ee572f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWallPlayerA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46c94f9e-71ad-4cea-86cb-fc10f77e6e0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c6ae428-a5f9-40e6-bf9a-a2381ee572f4",
            "compositeImage": {
                "id": "53b3eb42-d865-4817-8594-a33afaa71e85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46c94f9e-71ad-4cea-86cb-fc10f77e6e0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1da4a1eb-d9a7-441b-aac4-3c46334271ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46c94f9e-71ad-4cea-86cb-fc10f77e6e0a",
                    "LayerId": "22ea6b4a-c5d7-49e1-bfa2-be277472c324"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "22ea6b4a-c5d7-49e1-bfa2-be277472c324",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c6ae428-a5f9-40e6-bf9a-a2381ee572f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}