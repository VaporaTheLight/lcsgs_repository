{
    "id": "44e9718b-d9af-4b81-b3b3-413c52bf004a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWallPlayerR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd7a9d50-1d70-4780-a6ed-5e1d702dbad2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44e9718b-d9af-4b81-b3b3-413c52bf004a",
            "compositeImage": {
                "id": "0045febc-f4b7-4d6d-b9f8-ecf637eec899",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd7a9d50-1d70-4780-a6ed-5e1d702dbad2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b4c9aed-b94c-4ff3-acfb-f5a173073082",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd7a9d50-1d70-4780-a6ed-5e1d702dbad2",
                    "LayerId": "4c776d1c-3526-40f8-a9ce-13994146d15d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4c776d1c-3526-40f8-a9ce-13994146d15d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44e9718b-d9af-4b81-b3b3-413c52bf004a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}