{
    "id": "97264904-c878-4950-bb64-22b8690570a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackground_Sewer_Tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03b6a11c-60e1-4180-9cac-edfe1b5ebc5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97264904-c878-4950-bb64-22b8690570a1",
            "compositeImage": {
                "id": "acfbac05-4d07-46f4-8b6e-cd17892206e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03b6a11c-60e1-4180-9cac-edfe1b5ebc5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e8e20aa-6c28-4c27-8681-4abc0e124c23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03b6a11c-60e1-4180-9cac-edfe1b5ebc5e",
                    "LayerId": "a90ae8ec-d191-4d4e-a80a-ab9771a336b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a90ae8ec-d191-4d4e-a80a-ab9771a336b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97264904-c878-4950-bb64-22b8690570a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}