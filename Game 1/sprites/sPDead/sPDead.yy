{
    "id": "6a84d6aa-05be-4b52-8d67-61ca29b345c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPDead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29be4487-77f7-45a0-8e73-9d646a6a039a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a84d6aa-05be-4b52-8d67-61ca29b345c1",
            "compositeImage": {
                "id": "d31bd6c3-dadb-410b-8e40-2d46a1a11d98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29be4487-77f7-45a0-8e73-9d646a6a039a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a58a8b9-32bb-4b87-ab70-31a34e9422e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29be4487-77f7-45a0-8e73-9d646a6a039a",
                    "LayerId": "7375535a-8d9c-4c23-9458-ba8b48b4d538"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7375535a-8d9c-4c23-9458-ba8b48b4d538",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a84d6aa-05be-4b52-8d67-61ca29b345c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}