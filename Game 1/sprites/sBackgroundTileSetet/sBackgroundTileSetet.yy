{
    "id": "6d3a600c-8d90-44b6-a6d9-c594ec17e269",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackgroundTileSetet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1092afb6-42d1-4cf7-9fc3-6a632a747ee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d3a600c-8d90-44b6-a6d9-c594ec17e269",
            "compositeImage": {
                "id": "d8a7162c-70d1-4ba7-bf55-b05daac85875",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1092afb6-42d1-4cf7-9fc3-6a632a747ee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d1f9be1-8297-47d2-bb8d-e66bd76fb57a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1092afb6-42d1-4cf7-9fc3-6a632a747ee2",
                    "LayerId": "3d921ca7-3875-410e-9c92-0adc0d7545f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3d921ca7-3875-410e-9c92-0adc0d7545f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d3a600c-8d90-44b6-a6d9-c594ec17e269",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}