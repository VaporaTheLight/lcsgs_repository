{
    "id": "7d82bdf3-4fab-4412-a47d-6508b7eb90fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6553cb9-9fd5-43ee-83da-63faa2cf2bca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d82bdf3-4fab-4412-a47d-6508b7eb90fc",
            "compositeImage": {
                "id": "4d12a258-275a-4aed-87af-791eb51f8ffd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6553cb9-9fd5-43ee-83da-63faa2cf2bca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9593b4e-2217-4a07-9aa7-5d0bb8633535",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6553cb9-9fd5-43ee-83da-63faa2cf2bca",
                    "LayerId": "6b58acec-dd21-417e-9d09-6446b67d2e8d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "6b58acec-dd21-417e-9d09-6446b67d2e8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d82bdf3-4fab-4412-a47d-6508b7eb90fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 0,
    "yorig": 0
}