{
    "id": "4c1ff968-9dbc-4b5f-a945-ead303185e83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWallPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ecb5b805-21bc-433e-bfdd-11102e7bdd37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c1ff968-9dbc-4b5f-a945-ead303185e83",
            "compositeImage": {
                "id": "d2943d1b-dc7f-40db-9ab6-734439eed558",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecb5b805-21bc-433e-bfdd-11102e7bdd37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "860239f3-4a8f-4aa3-8494-56f4ffcee2e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecb5b805-21bc-433e-bfdd-11102e7bdd37",
                    "LayerId": "e457e0f9-c0f1-4c27-a792-44e73e4b160a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e457e0f9-c0f1-4c27-a792-44e73e4b160a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c1ff968-9dbc-4b5f-a945-ead303185e83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}