{
    "id": "c3c37906-02c6-47e9-8cf4-e6e1aa5b81ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "04d98f41-81a4-43d8-9180-a0388221ac9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3c37906-02c6-47e9-8cf4-e6e1aa5b81ca",
            "compositeImage": {
                "id": "2961038b-7369-4eb1-9bb2-207e6fa968dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04d98f41-81a4-43d8-9180-a0388221ac9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c11cc6d5-e583-4def-a6c5-b803dc994730",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04d98f41-81a4-43d8-9180-a0388221ac9c",
                    "LayerId": "03a6880f-6085-4183-a350-eb9d393f6cf5"
                }
            ]
        },
        {
            "id": "bffcbc9a-c28a-421a-a2b5-9558f71f177a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3c37906-02c6-47e9-8cf4-e6e1aa5b81ca",
            "compositeImage": {
                "id": "7eeba695-11f3-4c57-9ab2-47546305c221",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bffcbc9a-c28a-421a-a2b5-9558f71f177a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1b0330e-d4a4-4bfa-b593-8ec8e55d2750",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bffcbc9a-c28a-421a-a2b5-9558f71f177a",
                    "LayerId": "03a6880f-6085-4183-a350-eb9d393f6cf5"
                }
            ]
        },
        {
            "id": "723de1f7-2503-4ec4-8683-b97b7c38d76b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3c37906-02c6-47e9-8cf4-e6e1aa5b81ca",
            "compositeImage": {
                "id": "31372b53-3bdc-470b-92b7-7b2b0bbeecdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "723de1f7-2503-4ec4-8683-b97b7c38d76b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c603c2c-d850-4aad-a3f9-4ccc62e29d66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "723de1f7-2503-4ec4-8683-b97b7c38d76b",
                    "LayerId": "03a6880f-6085-4183-a350-eb9d393f6cf5"
                }
            ]
        },
        {
            "id": "3d81a7ba-450c-4a9b-8221-1b25f42bc79a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3c37906-02c6-47e9-8cf4-e6e1aa5b81ca",
            "compositeImage": {
                "id": "4afec7e9-598b-440b-b255-db12654db5f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d81a7ba-450c-4a9b-8221-1b25f42bc79a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce97d3bf-d997-4d9b-b2a3-6096e9744cf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d81a7ba-450c-4a9b-8221-1b25f42bc79a",
                    "LayerId": "03a6880f-6085-4183-a350-eb9d393f6cf5"
                }
            ]
        },
        {
            "id": "ce0f01df-6e02-4bc3-a17b-a92a70ba5148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3c37906-02c6-47e9-8cf4-e6e1aa5b81ca",
            "compositeImage": {
                "id": "8e3dd696-a846-4310-921d-baf49b4daceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce0f01df-6e02-4bc3-a17b-a92a70ba5148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df487f77-9a26-4308-88f2-b6f91166d520",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce0f01df-6e02-4bc3-a17b-a92a70ba5148",
                    "LayerId": "03a6880f-6085-4183-a350-eb9d393f6cf5"
                }
            ]
        },
        {
            "id": "40b56249-6711-427b-b33c-f9cc5ad51ea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3c37906-02c6-47e9-8cf4-e6e1aa5b81ca",
            "compositeImage": {
                "id": "f1a8bd6b-d39f-4a7d-8b31-08a83e66984f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40b56249-6711-427b-b33c-f9cc5ad51ea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4b7ba14-ddf5-457b-bb7e-e0c7ebeea4c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40b56249-6711-427b-b33c-f9cc5ad51ea9",
                    "LayerId": "03a6880f-6085-4183-a350-eb9d393f6cf5"
                }
            ]
        },
        {
            "id": "3d44e406-ac46-40bd-bb5d-a0b25d3be20b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3c37906-02c6-47e9-8cf4-e6e1aa5b81ca",
            "compositeImage": {
                "id": "d31ed5ec-46ea-4cc4-8833-61b1f137d7ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d44e406-ac46-40bd-bb5d-a0b25d3be20b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "721c6677-7384-44aa-af31-f749c38df758",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d44e406-ac46-40bd-bb5d-a0b25d3be20b",
                    "LayerId": "03a6880f-6085-4183-a350-eb9d393f6cf5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "03a6880f-6085-4183-a350-eb9d393f6cf5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3c37906-02c6-47e9-8cf4-e6e1aa5b81ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 0,
    "yorig": 0
}