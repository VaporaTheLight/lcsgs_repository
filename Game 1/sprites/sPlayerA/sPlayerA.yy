{
    "id": "8b9701b8-b42d-4222-b86f-26e9a88fa47c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "361288e0-fc6e-4e70-a35d-d47b0113ba5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b9701b8-b42d-4222-b86f-26e9a88fa47c",
            "compositeImage": {
                "id": "2be4a715-751e-4d8f-995a-4fca18193b0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "361288e0-fc6e-4e70-a35d-d47b0113ba5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c806174-efd6-493e-9177-792fa84d0910",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "361288e0-fc6e-4e70-a35d-d47b0113ba5a",
                    "LayerId": "d8e8e777-100a-4615-94cf-68bea7986b86"
                }
            ]
        },
        {
            "id": "54e6cf41-3b8a-4ec6-94f2-0f2cb8519036",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b9701b8-b42d-4222-b86f-26e9a88fa47c",
            "compositeImage": {
                "id": "386f8787-23a8-42fc-abbd-df69dad51b74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54e6cf41-3b8a-4ec6-94f2-0f2cb8519036",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdec8704-236e-4db1-bcbc-7e2ef6c42ebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54e6cf41-3b8a-4ec6-94f2-0f2cb8519036",
                    "LayerId": "d8e8e777-100a-4615-94cf-68bea7986b86"
                }
            ]
        },
        {
            "id": "1da6215f-3a21-4434-84a0-b5af8fd26972",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b9701b8-b42d-4222-b86f-26e9a88fa47c",
            "compositeImage": {
                "id": "0410992b-8289-48b1-a0f7-10d179379275",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1da6215f-3a21-4434-84a0-b5af8fd26972",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef628e77-329a-4ef6-b305-044a5cc4eb65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1da6215f-3a21-4434-84a0-b5af8fd26972",
                    "LayerId": "d8e8e777-100a-4615-94cf-68bea7986b86"
                }
            ]
        },
        {
            "id": "5a2f415e-3166-4efc-bce0-6ae68c880edb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b9701b8-b42d-4222-b86f-26e9a88fa47c",
            "compositeImage": {
                "id": "809ce4f3-151d-405b-86d8-85ef81a67da3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a2f415e-3166-4efc-bce0-6ae68c880edb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5ad0565-9778-4bc3-8884-ae1f56ff94bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a2f415e-3166-4efc-bce0-6ae68c880edb",
                    "LayerId": "d8e8e777-100a-4615-94cf-68bea7986b86"
                }
            ]
        },
        {
            "id": "a07f3f21-d6e0-4129-9598-93e740d16f77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b9701b8-b42d-4222-b86f-26e9a88fa47c",
            "compositeImage": {
                "id": "30fb26cf-220f-4efd-b6b9-5162dae2e0e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a07f3f21-d6e0-4129-9598-93e740d16f77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06f04b7c-e786-4a83-a7cc-8a94f8875662",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a07f3f21-d6e0-4129-9598-93e740d16f77",
                    "LayerId": "d8e8e777-100a-4615-94cf-68bea7986b86"
                }
            ]
        },
        {
            "id": "99ef0804-f028-4bc2-ab3f-881f2c1b0495",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b9701b8-b42d-4222-b86f-26e9a88fa47c",
            "compositeImage": {
                "id": "a7d0a394-8db8-46c6-8300-8835c943b2e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99ef0804-f028-4bc2-ab3f-881f2c1b0495",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dbe1304-534f-4f81-b5f4-81874a58d66a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99ef0804-f028-4bc2-ab3f-881f2c1b0495",
                    "LayerId": "d8e8e777-100a-4615-94cf-68bea7986b86"
                }
            ]
        },
        {
            "id": "64a7e3a6-aadb-48b6-858c-8448dd5f28d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b9701b8-b42d-4222-b86f-26e9a88fa47c",
            "compositeImage": {
                "id": "c7ef9648-2422-41c0-aca3-f9e9201b735b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64a7e3a6-aadb-48b6-858c-8448dd5f28d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43acfb42-d3f6-4bab-a95c-5cfd6996befc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64a7e3a6-aadb-48b6-858c-8448dd5f28d6",
                    "LayerId": "d8e8e777-100a-4615-94cf-68bea7986b86"
                }
            ]
        },
        {
            "id": "25630f8d-4a14-4f29-a5eb-c97f4d29c112",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b9701b8-b42d-4222-b86f-26e9a88fa47c",
            "compositeImage": {
                "id": "d271b84b-28d1-4d25-9213-98a7143d7af8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25630f8d-4a14-4f29-a5eb-c97f4d29c112",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "decb3ac2-f005-4b73-b3de-13afaad8484f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25630f8d-4a14-4f29-a5eb-c97f4d29c112",
                    "LayerId": "d8e8e777-100a-4615-94cf-68bea7986b86"
                }
            ]
        },
        {
            "id": "d01e9b3b-21c6-480d-bfce-d757e7d95990",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b9701b8-b42d-4222-b86f-26e9a88fa47c",
            "compositeImage": {
                "id": "03cfbc03-94b8-4408-bd7f-93c3a9fca1b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d01e9b3b-21c6-480d-bfce-d757e7d95990",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d1dfb00-db55-4526-bd46-606a5cdd9796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d01e9b3b-21c6-480d-bfce-d757e7d95990",
                    "LayerId": "d8e8e777-100a-4615-94cf-68bea7986b86"
                }
            ]
        },
        {
            "id": "f106ed08-f382-42fe-bced-054ec2236c3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b9701b8-b42d-4222-b86f-26e9a88fa47c",
            "compositeImage": {
                "id": "9c7328bf-98b3-4144-baec-b79b43918694",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f106ed08-f382-42fe-bced-054ec2236c3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "290d0773-2fe7-4d63-aa95-def2ef00800b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f106ed08-f382-42fe-bced-054ec2236c3f",
                    "LayerId": "d8e8e777-100a-4615-94cf-68bea7986b86"
                }
            ]
        },
        {
            "id": "c3f6a746-0c12-4a4e-86c5-1d094e2197d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b9701b8-b42d-4222-b86f-26e9a88fa47c",
            "compositeImage": {
                "id": "95255a7e-ea94-4ed2-9894-4ee8045fad09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3f6a746-0c12-4a4e-86c5-1d094e2197d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bb1ea85-215d-49ad-97df-fe30f528a36c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3f6a746-0c12-4a4e-86c5-1d094e2197d5",
                    "LayerId": "d8e8e777-100a-4615-94cf-68bea7986b86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "d8e8e777-100a-4615-94cf-68bea7986b86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b9701b8-b42d-4222-b86f-26e9a88fa47c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}