{
    "id": "357ac09d-0b7a-4951-bdc8-32d73643e1d6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDashPlayer",
    "eventList": [
        {
            "id": "08a11d2b-46bd-4ccc-badd-b234de2df753",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "357ac09d-0b7a-4951-bdc8-32d73643e1d6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6aaf3b4f-5927-4156-89bd-c07ce48d4ff5",
    "visible": true
}