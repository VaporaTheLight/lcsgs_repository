{
    "id": "7f9eb3b9-9e59-46ef-a127-6b89bdc23cfd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEDead",
    "eventList": [
        {
            "id": "c55fb485-a0c3-4d80-885e-e3155d5ab329",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7f9eb3b9-9e59-46ef-a127-6b89bdc23cfd"
        },
        {
            "id": "6048583f-e72d-4139-a730-c798bb0e898b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7f9eb3b9-9e59-46ef-a127-6b89bdc23cfd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "18bcdf6d-8ec0-4d67-bf0b-d26e4bca6794",
    "visible": true
}