{
    "id": "840e4c12-74cc-4558-bacd-d9ca0e8a4d58",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlDead",
    "eventList": [
        {
            "id": "deb48a54-1ddc-4d56-9014-7761eff469d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "840e4c12-74cc-4558-bacd-d9ca0e8a4d58"
        },
        {
            "id": "6406ab8a-7bb9-45d6-9e9e-9fa5effd5a73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "840e4c12-74cc-4558-bacd-d9ca0e8a4d58"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6a84d6aa-05be-4b52-8d67-61ca29b345c1",
    "visible": true
}