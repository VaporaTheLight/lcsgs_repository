{
    "id": "bb4b0669-e617-4018-bc5b-7a142544ca33",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer1",
    "eventList": [
        {
            "id": "6e604f1a-980d-4ff9-9697-a4f1b4654a1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bb4b0669-e617-4018-bc5b-7a142544ca33"
        },
        {
            "id": "37571b94-c3d7-470e-bfb3-dffa9b9b6cae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bb4b0669-e617-4018-bc5b-7a142544ca33"
        },
        {
            "id": "eefc1709-bf2b-480d-8b9e-efaf3daf4136",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bb4b0669-e617-4018-bc5b-7a142544ca33"
        },
        {
            "id": "1e75738c-2f4e-45b5-aadb-c144281c26d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "bb4b0669-e617-4018-bc5b-7a142544ca33"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "21a18e37-df8d-4b24-af17-fcc32aa10e7c",
    "visible": true
}