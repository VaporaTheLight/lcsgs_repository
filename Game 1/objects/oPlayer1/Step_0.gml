//Dash
if(mouse_check_button_released(mb_left))
{
	//Create Path
	var path = path_add();
	path_set_closed(path, false);
	path_add_point(path, x, y, 0);
	path_add_point(path, mouse_x, mouse_y, 0);


	//Distance
	var distance = round(point_distance(x, y, mouse_x, mouse_y));
	//Amount of Ghosts
	var ghostsToMake = ceil(distance/sprite_width);
	
	for(var i = .96; i >= 0; i -= .9/ ghostsToMake)
	{
		var ghost = instance_create_depth(path_get_x(path, i), path_get_y(path, i), depth, oDashPlayer);
		ghost.image_alpha = i;
	}
	
	//Move Player1
	x = mouse_x;
	y = mouse_y;
	
	//Animate
	image_index = image_number - 1;
	
	//Destroy Path
	path_delete(path);
	
	//Reset Animation
	alarm[0] = 40;

}
