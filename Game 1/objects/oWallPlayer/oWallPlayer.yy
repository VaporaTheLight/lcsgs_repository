{
    "id": "fe9d97a2-f9d8-44f5-9001-5122b8a1709c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWallPlayer",
    "eventList": [
        {
            "id": "72cff298-f7aa-4b40-8940-4de132c6399b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fe9d97a2-f9d8-44f5-9001-5122b8a1709c"
        },
        {
            "id": "20940ad0-b2b9-418c-93bc-f7977ea5da84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fe9d97a2-f9d8-44f5-9001-5122b8a1709c"
        },
        {
            "id": "06de3dba-7045-412c-a767-15bc77f5d7a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "fe9d97a2-f9d8-44f5-9001-5122b8a1709c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}