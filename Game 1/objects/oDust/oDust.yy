{
    "id": "bdf1da41-5b50-4107-8808-f6725336d001",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDust",
    "eventList": [
        {
            "id": "e47be972-a102-475e-8599-2fa902cc851f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bdf1da41-5b50-4107-8808-f6725336d001"
        },
        {
            "id": "d4a147f9-4023-4a05-ae41-580009bbe6b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "bdf1da41-5b50-4107-8808-f6725336d001"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "858e2b5d-90d8-4360-86b9-95ca7b9bf88a",
    "visible": true
}