draw_self();

if !instance_exists(oPlayer) exit;

draw_hp = lerp(draw_hp, oPlayer.hp, .25);

draw_set_color(c_red);
draw_rectangle(x + 4, y + 4, x + 123 * draw_hp/oHPBar.max_hp, y + 11, false);
draw_set_color(c_white);
