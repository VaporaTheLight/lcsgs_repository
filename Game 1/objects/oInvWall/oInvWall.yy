{
    "id": "9925c5af-b105-4115-b570-eac674596d5e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oInvWall",
    "eventList": [
        {
            "id": "fc111878-77f1-4232-8dc2-c9129cae5762",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "adc07d2f-a3a7-4e09-994e-3212583be5eb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9925c5af-b105-4115-b570-eac674596d5e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "f1ab99f5-c1c3-40fe-9fb3-3e46c2365061",
    "visible": false
}