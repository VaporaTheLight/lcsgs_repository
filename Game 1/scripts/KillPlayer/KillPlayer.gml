//Die
instance_change(oPlDead, true);

direction = point_direction(other.x,other.y,x,y);
hsp = lengthdir_x(3,direction);
vsp = lengthdir_y(4,direction)-2;
if (sign(hsp) != 0) image_xscale = sign(hsp);

global.kills -= global.killsinthisroom;
global.hp1 = 0;
global.Player_lives = 5;