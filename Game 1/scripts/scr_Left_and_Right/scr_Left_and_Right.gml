//Player Movements
key_left = keyboard_check(vk_left) || keyboard_check(ord ("A"));
key_right= keyboard_check(vk_right) || keyboard_check(ord ("D"));
key_jump = keyboard_check_pressed(vk_space) || keyboard_check(ord ("Z"));

//Calculate Movement
var move = key_right - key_left;
hsp = move *  walkspd; 
if (hsp != 0)  image_xscale = sign(hsp);
vsp = vsp + grv;

if(key_left)
{
	x += hsp;
}

else if(key_right)
{
	x += hsp;
}
