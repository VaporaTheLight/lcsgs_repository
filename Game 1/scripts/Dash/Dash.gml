//React to inputs
move = key_left + key_right
hsp = move * movespeed;
if (vsp < 10) {
    vsp += grv;
}

if (DashTimer = true) {
    SecondsSinceFirstDashTap = SecondsSinceFirstDashTap + 1/room_speed
}

if (dashing = true) {
    hsp = move * dashspeed
} else {
    hsp = move * movespeed
}

// Check if left or right is double-tapped
if (keyboard_check_pressed(vk_left) || keyboard_check_pressed(vk_right)) { 
    DashTimer = true
    if (SecondsSinceFirstDashTap >= SecondsBetweenDashTaps) {
        DashTimer = false
        SecondsSinceFirstDashTap = 0
    } else {
        dashing = true
        SecondsSinceFirstDashTap = 0
    }
} 
