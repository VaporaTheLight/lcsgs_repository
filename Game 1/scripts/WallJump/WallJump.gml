//Calc Horizontal Movent
walljumpdelay = max(walljumpdelay - 1, 0);
if (walljumpdelay == 0)  
	{
	var dir = key_right - key_left;
	hsp += dir * hsp_acc;
if(dir == 0)
	{
	var hsp_fric_final = hsp_fric_ground;
	if(!onground) hsp_fric_final = hsp_fric_air;
	hsp = Approach(hsp, 0, hsp_fric_final);
	}
hsp = clamp(hsp, -hsp_walk, hsp_walk);
}

//Calc Vertical Movement
var grv_final = grv;
var vsp_max_final = vsp_max;
if (onwall != 0) && (vsp > 0)
	{
	grv_final = grv_wall;
	vsp_max_final = vsp_max_wall;
	}
vsp += grv_final;
vsp = clamp(vsp, -vsp_max_final, vsp_max_final);

//Wall Jump
if (onwall != 0) && (!onground) && (key_jump)
	{
	walljumpdelay = walljumpdelay_max;
	hsp = -onwall * hsp_wjump;
	vsp = vsp_wjump;
	hsp_frac = 0;
	vsp_frac = 0;
	}

//Ground Jump
if(jumpbuffer > 0)
	{
	jumpbuffer--;
if(key_jump)
	{
	jumpbuffer = 0;
	vsp = vsp_jump;
	vsp_fric = 0;
	}
	}
vsp = clamp(vsp, -vsp_max, vsp_max);

//Dump Fraction and Get final Integer Speed
hsp += hsp_frac;
vsp += vsp_frac;
hsp_frac = frac(hsp);
vsp_frac = frac(vsp);
hsp -= hsp_frac;
vsp -= vsp_frac;

//Calc Current Status
onground = place_meeting(x, y + 1, oWall);
onwall = place_meeting(x + 1, y, oWall) - place_meeting(x - 1, y, oWall);
if (onground) jumpbuffer = 6;

//Adjust Sprite
image_speed = 1;
if (hsp != 0) image_xscale = sign(hsp);
if (!onground)
	{
	if (onwall != 0)
	{
	sprite_index = sPlayer;
	var side = bbox_left;
if(onwall == 1) side = bbox_right;
dust++;
if ((dust > 2) && (vsp > 0)) 
	{
	other.dust = 0;
	hspeed = -other.onwall * 0.5;                                
	}
	}
	else
	{
	sprite_index = sWallPlayerA;
	image_speed = 0;
	image_index = (vsp > 0);
	}
	}
	else
	{
if (hsp != 0) sprite_index = sPlayerR
else sprite_index = sPlayer;
}


