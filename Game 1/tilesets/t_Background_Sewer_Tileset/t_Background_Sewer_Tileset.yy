{
    "id": "ab5634e7-f10a-4cc1-b4a3-5511631b0b40",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "t_Background_Sewer_Tileset",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 4,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "97264904-c878-4950-bb64-22b8690570a1",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 16,
    "tileheight": 16,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 16,
    "tilexoff": 0,
    "tileyoff": 0
}