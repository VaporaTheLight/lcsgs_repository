if(hasPressed_W && !obj_player.hasLandedCloud)
{
	obj_player.glide_Grv = 0.5;
	grv_Glide_Timer -= 1;
	if(grv_Glide_Timer <= 0)
	{
		hasPressed_W = false;
	}
}
else
{
	hasPressed_W = false;
	obj_player.glide_Grv = 1.0;
	grv_Glide_Timer = room_speed * 2;
}
