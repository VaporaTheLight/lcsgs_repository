// checks if player lands on cloud below
if(place_meeting(x,y + total_grv,obj_cloud))
{
	// Get Cloud ID and assign it to a variable
	scr_Get_Cloud_ID();
	cloud_ID = scr_Get_Cloud_ID();
	
	// Checks if the player landed
	hasLandedCloud = true;
	if(hasLandedCloud)
	{
		// starts the cloud timer
		cloud_ID.cloud_timer -= 1;
		// Play Cloud Damaged State	 
		scr_anim_Play_Cloud_Damged(cloud_ID);
		// When the time is up, the cloud is destroyed and resets the timer
		if(cloud_ID.cloud_timer <= 0)
		{
			instance_destroy(cloud_ID);
			//timer = room_speed;
		}
	}
}
// if the player hasnt land on cloud below, also resets the cloud timer
else
{
	hasLandedCloud = false;
	//timer = room_speed;
}