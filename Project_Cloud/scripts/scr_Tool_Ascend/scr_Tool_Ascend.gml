if(hasPressed_Q)
{
	with(obj_player)
	{
		y -= 20;
	}

	grv_Ascend_Timer -= 1;
	if(grv_Ascend_Timer <= 0)
	{
		hasPressed_Q = false;
	}
}
else
{
	hasPressed_Q = false;
	grv_Ascend_Timer = room_speed/5;
}