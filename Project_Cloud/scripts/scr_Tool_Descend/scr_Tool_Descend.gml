if(hasPressed_S)
{
	with(obj_player)
	{
		y += 20;
	}

	grv_Descend_Timer -= 1;
	if(grv_Descend_Timer <= 0)
	{
		hasPressed_S = false;
	}
}
else
{
	hasPressed_S = false;
	grv_Descend_Timer = room_speed/5;
}