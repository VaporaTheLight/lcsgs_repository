// Set key to variables
keypad_right = keyboard_check(vk_right);
keypad_left = keyboard_check(vk_left);
right_collision = place_meeting(x + hsp ,y,obj_cloud);
left_collision = place_meeting(x - hsp ,y,obj_cloud);

// check right collision
if(keypad_right && !right_collision)
{
	// run right
	if(keyboard_check(vk_shift) && !right_collision)
	{
		// move
		x += hsp * 2.0;
	}
	// move
	hsp = move_spd ;
	x += hsp;
}

// check left collision
if(keypad_left && !left_collision)
{
	// run right
	if(keyboard_check(vk_shift) && !left_collision)
	{
		// move
		x += -hsp * 2.0;
	}
	// move 
	hsp = move_spd;  
	x += -hsp;
}

