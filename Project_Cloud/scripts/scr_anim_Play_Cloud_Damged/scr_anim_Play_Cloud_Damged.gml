// takes in cloud id from scr_Get_Cloud_ID
var cloud_temp_ID = argument0;

// checks to see if the cloud_ID.cloud_timer is below a certain percentage	
if(cloud_ID.cloud_timer <= cloud_temp_ID.cloud_state_percentage )
{
	// set unique cloud ID's properties
	with(cloud_temp_ID)
	{
		/* checks to see if cloud_state_stages is less than the array length of cloud_temp_ID.cloud_States
		 and if true, cloud can change sprites depending on the value on array[]*/
		if(cloud_temp_ID.cloud_state_stages < array_length_1d(cloud_temp_ID.cloud_States))
		{
			sprite_index = cloud_States[cloud_temp_ID.cloud_state_stages];
		}
		// increment cloud_temp_ID.cloud_state_stages to change the cloud array element
		cloud_temp_ID.cloud_state_stages++;
		/* cloud_state_percentage is divided by 2 so that the cloud can change its sprite
		 and serves as a condition to change a sprite index*/
		cloud_temp_ID.cloud_state_percentage = cloud_temp_ID.cloud_state_percentage/2;
	}
}
	
