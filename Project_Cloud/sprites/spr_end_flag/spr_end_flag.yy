{
    "id": "9cf77e11-f02f-4381-88c3-624fe39fb08e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_end_flag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d646e65-ab49-4ba7-95b2-f5a53304d891",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cf77e11-f02f-4381-88c3-624fe39fb08e",
            "compositeImage": {
                "id": "e10ba1a1-e0e3-4edf-a73d-8a898ec57a56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d646e65-ab49-4ba7-95b2-f5a53304d891",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69a02cef-d986-4fae-9ad0-9c757688a03a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d646e65-ab49-4ba7-95b2-f5a53304d891",
                    "LayerId": "e4684cfa-f992-4249-8d67-2ce2291e0ffd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e4684cfa-f992-4249-8d67-2ce2291e0ffd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cf77e11-f02f-4381-88c3-624fe39fb08e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}