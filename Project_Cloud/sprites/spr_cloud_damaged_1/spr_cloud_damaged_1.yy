{
    "id": "726020f4-2ede-4763-b05f-789dbed1a1b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cloud_damaged_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9339ebba-14a2-4d5e-a5fb-94ad79dae167",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "726020f4-2ede-4763-b05f-789dbed1a1b2",
            "compositeImage": {
                "id": "a80b91cf-fb5f-4874-8865-03d3a11436ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9339ebba-14a2-4d5e-a5fb-94ad79dae167",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e6e0022-7863-44ac-8d4d-63e55a9eae29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9339ebba-14a2-4d5e-a5fb-94ad79dae167",
                    "LayerId": "ab284f1d-4665-4952-917b-074bbd140949"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ab284f1d-4665-4952-917b-074bbd140949",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "726020f4-2ede-4763-b05f-789dbed1a1b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}