{
    "id": "5b71aeaa-3bb6-487b-b0c6-fa62727592b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemies",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e311576c-acac-485d-891c-61873dfdde1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b71aeaa-3bb6-487b-b0c6-fa62727592b8",
            "compositeImage": {
                "id": "400c1b34-e6ff-413a-890a-e914e688d164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e311576c-acac-485d-891c-61873dfdde1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f68a101-2285-491b-9acc-98dd3867f47a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e311576c-acac-485d-891c-61873dfdde1c",
                    "LayerId": "1c59eded-dbb5-4370-b60c-8d346ae9bf87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1c59eded-dbb5-4370-b60c-8d346ae9bf87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b71aeaa-3bb6-487b-b0c6-fa62727592b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}