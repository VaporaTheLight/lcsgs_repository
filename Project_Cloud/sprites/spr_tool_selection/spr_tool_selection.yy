{
    "id": "eb252ee9-9f17-48a5-95e7-48ff1883fce8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tool_selection",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff985eac-f7e1-41ec-9e1f-b30670b7264f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb252ee9-9f17-48a5-95e7-48ff1883fce8",
            "compositeImage": {
                "id": "e71cc78e-2296-420d-a5af-a9584e0c527f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff985eac-f7e1-41ec-9e1f-b30670b7264f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37e53745-b559-439f-b561-9cdcb8649f2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff985eac-f7e1-41ec-9e1f-b30670b7264f",
                    "LayerId": "fbd1fc1e-22dc-4a5c-90e1-71713829bfbf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fbd1fc1e-22dc-4a5c-90e1-71713829bfbf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb252ee9-9f17-48a5-95e7-48ff1883fce8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}