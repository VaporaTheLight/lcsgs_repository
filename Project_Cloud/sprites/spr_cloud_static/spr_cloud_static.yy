{
    "id": "7d195f8d-5e32-4545-8e98-2ee9f6af0bc0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cloud_static",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "602e302d-8b45-48bf-aa60-048c3c82b245",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d195f8d-5e32-4545-8e98-2ee9f6af0bc0",
            "compositeImage": {
                "id": "c795589a-954f-4ba8-8a15-1e5dc94e79fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "602e302d-8b45-48bf-aa60-048c3c82b245",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfe60aa9-0361-4132-8280-8f0bb5fc784d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "602e302d-8b45-48bf-aa60-048c3c82b245",
                    "LayerId": "2b8dd9a4-1a02-42e1-8a7e-18538b201310"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2b8dd9a4-1a02-42e1-8a7e-18538b201310",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d195f8d-5e32-4545-8e98-2ee9f6af0bc0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}