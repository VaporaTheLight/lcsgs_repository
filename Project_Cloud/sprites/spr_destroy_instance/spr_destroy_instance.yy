{
    "id": "1cf6efdb-a8dd-49f8-99a2-7e88695f528c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_destroy_instance",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "caed3a34-7260-4c68-95af-cfb673f57f26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf6efdb-a8dd-49f8-99a2-7e88695f528c",
            "compositeImage": {
                "id": "06583a6b-3b5d-438a-8835-f1aa59b9a171",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caed3a34-7260-4c68-95af-cfb673f57f26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df276250-a4ae-4989-8cf3-0998b1fc599a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caed3a34-7260-4c68-95af-cfb673f57f26",
                    "LayerId": "9096eded-12e0-48bc-8db3-0ba228657ca5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9096eded-12e0-48bc-8db3-0ba228657ca5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1cf6efdb-a8dd-49f8-99a2-7e88695f528c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}