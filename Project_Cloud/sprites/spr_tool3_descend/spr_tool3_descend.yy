{
    "id": "34e7efeb-7350-4d6c-ba57-f69d89934513",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tool3_descend",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85165fc1-1412-484f-a57d-a991150f754d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34e7efeb-7350-4d6c-ba57-f69d89934513",
            "compositeImage": {
                "id": "914d0611-c00f-4c42-b12c-625ec1c65f5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85165fc1-1412-484f-a57d-a991150f754d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8d2e4a7-2f40-424e-87db-1a5ff4dc331e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85165fc1-1412-484f-a57d-a991150f754d",
                    "LayerId": "9fe7e912-3cdd-47b0-b4e6-48a9b3caa3a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9fe7e912-3cdd-47b0-b4e6-48a9b3caa3a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34e7efeb-7350-4d6c-ba57-f69d89934513",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}