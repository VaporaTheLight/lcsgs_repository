{
    "id": "073a57dd-11b4-4737-a003-391734574b25",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tool2_ascend",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e09694e0-a91c-4c19-8bd9-bd61d0af6e6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "073a57dd-11b4-4737-a003-391734574b25",
            "compositeImage": {
                "id": "cda1a3f3-75c2-4c29-a165-bf77fb3535f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e09694e0-a91c-4c19-8bd9-bd61d0af6e6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a43a4f90-6057-433b-a0fc-4ae508001b71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e09694e0-a91c-4c19-8bd9-bd61d0af6e6d",
                    "LayerId": "00859a3f-6e03-4951-97c8-a470badaf5b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "00859a3f-6e03-4951-97c8-a470badaf5b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "073a57dd-11b4-4737-a003-391734574b25",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}