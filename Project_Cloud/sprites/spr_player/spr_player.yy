{
    "id": "7727f2c8-6ce5-4c1f-8218-53b94e09d7a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82418c7b-e118-4947-9070-e2d27ae324de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7727f2c8-6ce5-4c1f-8218-53b94e09d7a6",
            "compositeImage": {
                "id": "5701c1d8-7f48-4bac-9815-e95f2262b7f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82418c7b-e118-4947-9070-e2d27ae324de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "834d38d5-d16f-45e3-80ab-77b0a28186e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82418c7b-e118-4947-9070-e2d27ae324de",
                    "LayerId": "1cab65d9-2c9b-460e-8c24-b897c6702073"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1cab65d9-2c9b-460e-8c24-b897c6702073",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7727f2c8-6ce5-4c1f-8218-53b94e09d7a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}