{
    "id": "a7fa024a-a60a-49b3-ac19-52921ef4c602",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tool1_glide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2a5ddbd-a866-4b62-8e88-405c624ee7e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7fa024a-a60a-49b3-ac19-52921ef4c602",
            "compositeImage": {
                "id": "e9954f61-71b6-4734-b1d1-5b4b6d759e19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2a5ddbd-a866-4b62-8e88-405c624ee7e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39f93442-8d81-4efe-8047-2b1aa3c027c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2a5ddbd-a866-4b62-8e88-405c624ee7e7",
                    "LayerId": "0b3c29fa-3b18-4254-afbf-0aa8a67681ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0b3c29fa-3b18-4254-afbf-0aa8a67681ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7fa024a-a60a-49b3-ac19-52921ef4c602",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}