{
    "id": "01ce0f2b-6931-4520-ad91-4a34a953d4c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hearts",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78b0a4e0-eb3b-4881-92dc-50080f713738",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01ce0f2b-6931-4520-ad91-4a34a953d4c1",
            "compositeImage": {
                "id": "c2f7f604-9603-465b-8783-af42f199e372",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78b0a4e0-eb3b-4881-92dc-50080f713738",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dba30dae-cf9c-4c5c-89ff-c0fdad4c5188",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78b0a4e0-eb3b-4881-92dc-50080f713738",
                    "LayerId": "0b14e637-182b-4f94-a829-9ba2dcd75b3e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0b14e637-182b-4f94-a829-9ba2dcd75b3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01ce0f2b-6931-4520-ad91-4a34a953d4c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}