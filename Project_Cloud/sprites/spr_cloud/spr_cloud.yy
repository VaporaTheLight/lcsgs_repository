{
    "id": "7d195f8d-5e32-4545-8e98-2ee9f6af0bc0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cloud",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e17b0ab8-9c23-496d-9ee0-67643fc0cfb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d195f8d-5e32-4545-8e98-2ee9f6af0bc0",
            "compositeImage": {
                "id": "f2e685cf-cbce-4dd0-9e52-a8f18432ecff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e17b0ab8-9c23-496d-9ee0-67643fc0cfb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29405d01-3350-44f4-8ca6-7350f6b8066c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e17b0ab8-9c23-496d-9ee0-67643fc0cfb6",
                    "LayerId": "38ce6926-a983-4c5b-894a-ed30f6defd3e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "38ce6926-a983-4c5b-894a-ed30f6defd3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d195f8d-5e32-4545-8e98-2ee9f6af0bc0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}