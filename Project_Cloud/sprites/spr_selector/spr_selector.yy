{
    "id": "bba56080-d9a4-408d-8e61-c98929d6638f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_selector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7ff99b2-95d2-4083-ae13-bfd5ce645ede",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bba56080-d9a4-408d-8e61-c98929d6638f",
            "compositeImage": {
                "id": "7bf4a0e2-1904-44d6-98ec-619ee1377bb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7ff99b2-95d2-4083-ae13-bfd5ce645ede",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c36b88f-87cc-4e1c-b257-bdd7aa6a8438",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7ff99b2-95d2-4083-ae13-bfd5ce645ede",
                    "LayerId": "1db411df-aad0-46a4-b9fe-6d9f3699be15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1db411df-aad0-46a4-b9fe-6d9f3699be15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bba56080-d9a4-408d-8e61-c98929d6638f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}