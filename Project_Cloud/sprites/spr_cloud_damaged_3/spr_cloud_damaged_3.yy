{
    "id": "ee12895f-4940-44ac-b6a7-a16f59b59769",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cloud_damaged_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e991ee04-1416-4968-92f5-8a3a7c82e232",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee12895f-4940-44ac-b6a7-a16f59b59769",
            "compositeImage": {
                "id": "f806347a-88c7-4e15-b9a1-59d19dd63bae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e991ee04-1416-4968-92f5-8a3a7c82e232",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a444664-476a-4fc7-bfb7-818803fe3cb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e991ee04-1416-4968-92f5-8a3a7c82e232",
                    "LayerId": "9287400f-bfa6-48ab-8e69-fef6d9940ded"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9287400f-bfa6-48ab-8e69-fef6d9940ded",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee12895f-4940-44ac-b6a7-a16f59b59769",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}