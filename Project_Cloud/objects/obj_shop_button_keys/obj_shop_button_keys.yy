{
    "id": "8be30af9-5eea-4e92-987b-1c39f9eda5c3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shop_button_keys",
    "eventList": [
        {
            "id": "6bbdbc2c-0186-4694-967a-8b734f8b8c6a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 81,
            "eventtype": 9,
            "m_owner": "8be30af9-5eea-4e92-987b-1c39f9eda5c3"
        },
        {
            "id": "c5e01e25-d1b3-4206-9751-cf6f548f72be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 9,
            "m_owner": "8be30af9-5eea-4e92-987b-1c39f9eda5c3"
        },
        {
            "id": "4eaae8bb-d174-4e3b-a345-c4c191618fcc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 69,
            "eventtype": 9,
            "m_owner": "8be30af9-5eea-4e92-987b-1c39f9eda5c3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}