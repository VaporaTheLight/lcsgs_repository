{
    "id": "6dd9512b-b708-4ce4-a79a-64afba066b23",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_manager_hp",
    "eventList": [
        {
            "id": "6bea4011-dbc5-494c-99b1-e2845b26381a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6dd9512b-b708-4ce4-a79a-64afba066b23"
        },
        {
            "id": "8a990a94-1ca1-4df6-9776-270faaf9bc57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6dd9512b-b708-4ce4-a79a-64afba066b23"
        },
        {
            "id": "77b60a73-d30c-432f-8381-ef4180e1c515",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "6dd9512b-b708-4ce4-a79a-64afba066b23"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}